use crate::entities::fun_iterator::{FunIterator, NextIter};

fn range_generator(begin: usize, end: usize) -> FunIterator<usize> {
    FunIterator::new(move || {
        if begin > end {
            return NextIter::Stop;
        }
        NextIter::Value(begin, range_generator(begin + 1, end))
    })
}

fn filter<T: 'static, F: Fn(&T) -> bool + 'static>(
    mut iter: FunIterator<T>,
    fun: F,
) -> FunIterator<T> {
    FunIterator::new(move || match iter.next() {
        None => NextIter::Stop,
        Some(val) if fun(&val) => NextIter::Value(val, filter(iter, fun)),
        Some(_) => NextIter::Next(filter(iter, fun)),
    })
}

#[test]
fn test_iterator() {
    let generator = FunIterator::from_vec(vec!["a", "b", "c"]);
    assert_eq!(generator.collect::<Vec<_>>(), vec!["a", "b", "c"])
}

#[test]
fn test_range() {
    let generator = range_generator(1, 5);
    assert_eq!(generator.collect::<Vec<_>>(), vec![1, 2, 3, 4, 5])
}

#[test]
fn test_filter() {
    let generator = range_generator(1, 10);
    let as_vec = filter(generator, |n| n % 2 != 0).collect::<Vec<_>>();
    assert_eq!(as_vec, &[1, 3, 5, 7, 9]);
}

#[test]
fn test_join() {
    let generator = range_generator(1, 3)
        .join(FunIterator::from_vec(vec![10, 1, 4]))
        .join(range_generator(6, 9));
    assert_eq!(
        generator.collect::<Vec<_>>(),
        vec![1, 2, 3, 10, 1, 4, 6, 7, 8, 9],
    )
}
