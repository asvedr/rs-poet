#[derive(Debug, Clone, Eq, PartialEq)]
pub struct NewPhrase {
    pub slug: String,
    pub text: String,
    pub ending: String,
    pub last_word: String,
    pub syl_count: usize,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Phrase {
    pub id: usize,
    pub slug: String,
    pub text: String,
    pub ending: String,
    pub last_word: String,
    pub syl_count: usize,
}

impl From<Phrase> for NewPhrase {
    fn from(src: Phrase) -> Self {
        Self {
            slug: src.slug,
            text: src.text,
            ending: src.ending,
            last_word: src.last_word,
            syl_count: src.syl_count,
        }
    }
}
