#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Word {
    pub text: String,
    pub syl_count: usize,
    pub ending: String,
}
