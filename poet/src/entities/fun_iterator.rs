use std::mem;

pub struct FunIterator<T> {
    root: Option<NextFun<T>>,
}

pub enum NextIter<T> {
    Stop,
    Value(T, FunIterator<T>),
    Single(T),
    Next(FunIterator<T>),
}

type NextFun<T> = Box<dyn FnOnce() -> NextIter<T>>;

fn execute_join<T: 'static>(fun: NextFun<T>, next_fun: NextFun<T>) -> NextIter<T> {
    let mut iterator = FunIterator::new(fun);
    let val = match iterator.next() {
        Some(val) => val,
        None => return next_fun(),
    };
    let iter_fun = match iterator.root {
        Some(fun) => make_join(fun, next_fun),
        None => next_fun,
    };
    NextIter::Value(val, FunIterator::new(iter_fun))
}

fn make_join<T: 'static>(fun: NextFun<T>, next_fun: NextFun<T>) -> NextFun<T> {
    Box::new(|| execute_join(fun, next_fun))
}

impl<T: 'static> FunIterator<T> {
    pub fn empty() -> Self {
        Self { root: None }
    }

    pub fn new<F: FnOnce() -> NextIter<T> + 'static>(fun: F) -> Self {
        Self {
            root: Some(Box::new(fun)),
        }
    }

    pub fn from_vec(mut vec: Vec<T>) -> Self {
        FunIterator::new(|| {
            if vec.is_empty() {
                return NextIter::Stop;
            }
            let val = vec.remove(0);
            NextIter::Value(val, Self::from_vec(vec))
        })
    }

    pub fn join(mut self, next: Self) -> Self {
        self.append(next);
        self
    }

    pub fn append(&mut self, next: Self) {
        let next_fun = match next.root {
            Some(val) => val,
            _ => return,
        };
        if let Some(fun) = mem::replace(&mut self.root, None) {
            self.root = Some(make_join(fun, next_fun))
        } else {
            self.root = Some(next_fun)
        }
    }

    pub fn yield_(self) -> NextIter<T> {
        match self.root {
            Some(fun) => fun(),
            _ => NextIter::Stop,
        }
    }
}

impl<T> Iterator for FunIterator<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        while let Some(fun) = mem::replace(&mut self.root, None) {
            match fun() {
                NextIter::Stop => return None,
                NextIter::Single(val) => return Some(val),
                NextIter::Value(val, next) => {
                    self.root = next.root;
                    return Some(val);
                }
                NextIter::Next(next) => self.root = next.root,
            }
        }
        None
    }
}
