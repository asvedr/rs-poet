use crate::entities::lang::Lang;
use std::str::FromStr;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum BaseType {
    Words,
    Phrases,
}

impl BaseType {
    const MAP: &'static [(Self, &'static str)] =
        &[(Self::Words, "words"), (Self::Phrases, "phrases")];
}

impl ToString for BaseType {
    fn to_string(&self) -> String {
        for (key, val) in Self::MAP {
            if key == self {
                return val.to_string();
            }
        }
        unreachable!()
    }
}

impl FromStr for BaseType {
    type Err = ();

    fn from_str(src: &str) -> Result<Self, Self::Err> {
        for (val, key) in Self::MAP {
            if *key == src {
                return Ok(*val);
            }
        }
        Err(())
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct BaseSettings {
    pub base_type: BaseType, // always words
    pub base_version: String,
    pub poet_version: String,
    pub lang: Lang,
    pub description: String,
}
