use std::str::FromStr;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Lang {
    Ru,
    En,
}

impl Default for Lang {
    fn default() -> Self {
        Self::En
    }
}

impl Lang {
    const MAP: &'static [(Self, &'static str)] = &[(Self::Ru, "ru"), (Self::En, "en")];
}

impl ToString for Lang {
    fn to_string(&self) -> String {
        for (key, val) in Self::MAP {
            if key == self {
                return val.to_string();
            }
        }
        unreachable!()
    }
}

impl FromStr for Lang {
    type Err = ();

    fn from_str(src: &str) -> Result<Self, Self::Err> {
        for (val, key) in Self::MAP {
            if *key == src {
                return Ok(*val);
            }
        }
        Err(())
    }
}
