use crate::entities::phrase::Phrase;

#[derive(Debug)]
pub struct GetPhraseRequest<'a, 'b> {
    pub(crate) syl_count: Option<usize>,
    pub(crate) rhyme_syl_count: Option<usize>,
    pub(crate) exclude: &'a [Phrase],
    pub(crate) exclude_rhyme: &'b [Phrase],
}

#[derive(Debug)]
pub struct GetRhymeRequest<'a> {
    pub(crate) syl_count: Option<usize>,
    pub(crate) exclude: &'a [Phrase],
}

impl<'a, 'b> GetPhraseRequest<'a, 'b> {
    pub fn new() -> GetPhraseRequest<'static, 'static> {
        GetPhraseRequest {
            syl_count: None,
            rhyme_syl_count: None,
            exclude: &[],
            exclude_rhyme: &[],
        }
    }

    pub fn syl_count(self, val: usize) -> GetPhraseRequest<'a, 'b> {
        GetPhraseRequest {
            syl_count: Some(val),
            rhyme_syl_count: self.rhyme_syl_count,
            exclude: self.exclude,
            exclude_rhyme: self.exclude_rhyme,
        }
    }

    pub fn new_rhyme_syl_count(self, val: usize) -> GetPhraseRequest<'a, 'b> {
        GetPhraseRequest {
            syl_count: self.syl_count,
            rhyme_syl_count: Some(val),
            exclude: self.exclude,
            exclude_rhyme: self.exclude_rhyme,
        }
    }

    pub fn exclude<'c>(self, val: &'c [Phrase]) -> GetPhraseRequest<'c, 'b> {
        GetPhraseRequest {
            syl_count: self.syl_count,
            rhyme_syl_count: self.rhyme_syl_count,
            exclude: val,
            exclude_rhyme: self.exclude_rhyme,
        }
    }

    pub fn exclude_new_rhymes<'c>(self, val: &'c [Phrase]) -> GetPhraseRequest<'a, 'c> {
        GetPhraseRequest {
            syl_count: self.syl_count,
            rhyme_syl_count: self.rhyme_syl_count,
            exclude: self.exclude,
            exclude_rhyme: val,
        }
    }
}

impl<'a> GetRhymeRequest<'a> {
    pub fn new() -> GetRhymeRequest<'static> {
        GetRhymeRequest {
            syl_count: None,
            exclude: &[],
        }
    }

    pub fn syl_count(self, val: usize) -> GetRhymeRequest<'a> {
        GetRhymeRequest {
            syl_count: Some(val),
            exclude: self.exclude,
        }
    }

    pub fn exclude(self, val: &[Phrase]) -> GetRhymeRequest {
        GetRhymeRequest {
            syl_count: self.syl_count,
            exclude: val,
        }
    }
}
