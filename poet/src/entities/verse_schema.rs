pub enum RowSizeSchema {
    Fixed(usize),
    Range {
        from: usize,
        to: usize,
    },
    Relative {
        row_index: usize,
        size_change: isize,
    },
}

pub struct VerseSchema {
    pub sizes: Vec<RowSizeSchema>,
    pub rhymes: Vec<(usize, usize)>,
}
