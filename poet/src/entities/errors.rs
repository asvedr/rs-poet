use std::io;
use std::io::Error;

#[derive(Debug)]
pub enum DictLoaderError {
    InvalidData(String),
    InvalidFilename(String),
    ExtProcessorNotFound(String),
    TooManyProcessorsForExt(String),
    IO(io::Error),
}

impl ToString for DictLoaderError {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}

pub type DLResult<T> = Result<T, DictLoaderError>;

#[derive(Debug)]
pub enum PhraseProcessorError {
    IO(io::Error),
    DbError(DbError),
    UnknownWord(String),
    InvalidParam(String),
    Filtered,
}

impl ToString for PhraseProcessorError {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}

impl Eq for PhraseProcessorError {}

impl PartialEq for PhraseProcessorError {
    fn eq(&self, other: &Self) -> bool {
        format!("{:?}", self) == format!("{:?}", other)
    }
}

pub type PPResult<T> = Result<T, PhraseProcessorError>;

impl From<io::Error> for DictLoaderError {
    fn from(src: Error) -> Self {
        Self::IO(src)
    }
}

impl From<DbError> for PhraseProcessorError {
    fn from(err: DbError) -> Self {
        Self::DbError(err)
    }
}

impl From<io::Error> for PhraseProcessorError {
    fn from(err: Error) -> Self {
        Self::IO(err)
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum DbError {
    FileNotExists,
    FileAlreadyExists,
    InvalidBaseType(String),
    RequiredValueNotFound(String),

    NotFound,
    TooManyValues,
    CanNotConnect(String),
    CanNotInitTable(&'static str, String),
    InvalidValueType(String),
    Other(String),
}

pub type DbResult<T> = Result<T, DbError>;

impl<T: ToString> From<T> for DbError {
    fn from(src: T) -> Self {
        Self::Other(src.to_string())
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum RandomizerError {
    CanNotOpenURandom,
    CanNotReadURandom,
}

#[derive(Debug, Eq, PartialEq)]
pub enum PoetError {
    MatchedRowsNotFound,
    InvalidSizeSchema,
    DbError(DbError),
    RandomizerError(RandomizerError),
    Other(String),
}

impl From<DbError> for PoetError {
    fn from(err: DbError) -> Self {
        PoetError::DbError(err)
    }
}

impl From<RandomizerError> for PoetError {
    fn from(err: RandomizerError) -> Self {
        PoetError::RandomizerError(err)
    }
}
