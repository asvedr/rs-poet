use std::collections::HashSet;
use std::path::Path;

use crate::entities::errors::DbError;

pub fn version() -> String {
    env!("CARGO_PKG_VERSION").to_string()
}

pub fn split_to_words(sentence: &str, alphabet: &HashSet<char>) -> Vec<Vec<char>> {
    let sentence = sentence.to_lowercase();
    let mut word = Vec::new();
    let mut result = Vec::new();
    for sym in sentence.chars() {
        if alphabet.contains(&sym) {
            word.push(sym);
        } else if !word.is_empty() {
            result.push(word);
            word = Vec::new();
        }
    }
    if !word.is_empty() {
        result.push(word)
    }
    result
}

pub fn ensure_file_exists(path: &Path) -> Result<(), DbError> {
    if path.exists() {
        Ok(())
    } else {
        Err(DbError::FileNotExists)
    }
}

pub fn ensure_file_not_exists(path: &Path) -> Result<(), DbError> {
    if path.exists() {
        Err(DbError::FileAlreadyExists)
    } else {
        Ok(())
    }
}
