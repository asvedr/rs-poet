mod entities;
mod impls;
mod proto;
mod utils;

use std::path::Path;

pub use crate::entities::errors::{DbError, DictLoaderError, PhraseProcessorError, PoetError};
pub use crate::entities::lang::Lang;
pub use crate::entities::phrase::{NewPhrase, Phrase};
pub use crate::entities::verse_schema::{RowSizeSchema, VerseSchema};
pub use crate::entities::word::Word;
use crate::impls::phrase_base::PhraseBase;
use crate::impls::sqlite_repos;
use crate::impls::word_base::WordBase;
use crate::utils::common::{ensure_file_exists, ensure_file_not_exists};

pub use crate::entities::fun_iterator::FunIterator;
use crate::impls::generator::Generator;
use crate::impls::phrase_processor::PhraseProcessor;
use crate::impls::universal_dict_loader::UniversalDictLoader;
pub use crate::proto::dict::IUniversalDictLoader;
use crate::proto::interface::IBaseLoader;
pub use crate::proto::interface::{IPhraseBase, IWordBase};
pub use crate::proto::phrase_processor::IPhraseProcessor;
pub use crate::proto::poet::IGenerator;
use crate::sqlite_repos::base::connection::SQLConnection;
use crate::sqlite_repos::phrase::DbPhraseRepo;
use crate::sqlite_repos::rhyme::DbRhymeRepo;
use crate::sqlite_repos::settings::DbSettingRepo;
use crate::sqlite_repos::word::DbWordRepo;
pub use crate::utils::common::version;

pub fn create_phrase_base<P: AsRef<Path>>(
    path: &P,
    lang: Lang,
    description: &str,
) -> Result<Box<dyn IPhraseBase>, DbError> {
    let path = path.as_ref();
    ensure_file_not_exists(path)?;
    let connect = SQLConnection::new(path)?;
    let base: PhraseBase<_, DbSettingRepo, DbPhraseRepo, DbRhymeRepo> =
        PhraseBase::create(connect, lang, description)?;
    Ok(Box::new(base))
}

pub fn load_phrase_base<P: AsRef<Path>>(path: &P) -> Result<Box<dyn IPhraseBase>, DbError> {
    let path = path.as_ref();
    ensure_file_exists(path)?;
    let connect = SQLConnection::new(path)?;
    let base: PhraseBase<_, DbSettingRepo, DbPhraseRepo, DbRhymeRepo> = PhraseBase::load(connect)?;
    Ok(Box::new(base))
}

pub fn create_word_base<P: AsRef<Path> + ?Sized>(
    path: &P,
    lang: Lang,
    description: &str,
) -> Result<Box<dyn IWordBase>, DbError> {
    let path = path.as_ref();
    ensure_file_not_exists(path)?;
    let connect = SQLConnection::new(path)?;
    let base: WordBase<_, DbSettingRepo, DbWordRepo> =
        WordBase::create(connect, lang, description)?;
    Ok(Box::new(base))
}

pub fn load_word_base<P: AsRef<Path>>(path: &P) -> Result<Box<dyn IWordBase>, DbError> {
    let path = path.as_ref();
    ensure_file_exists(path)?;
    let connect = SQLConnection::new(path)?;
    let base: WordBase<_, DbSettingRepo, DbWordRepo> = WordBase::load(connect)?;
    Ok(Box::new(base))
}

pub fn create_dict_loader() -> Box<dyn IUniversalDictLoader> {
    Box::new(UniversalDictLoader::default())
}

pub fn create_phrase_processor(
    word_base: Box<dyn IWordBase>,
) -> Result<Box<dyn IPhraseProcessor>, PhraseProcessorError> {
    Ok(Box::new(PhraseProcessor::new(word_base)))
}

pub fn create_generator(
    phrase_base: Box<dyn IPhraseBase>,
) -> Result<Box<dyn IGenerator>, PoetError> {
    Ok(Box::new(Generator::new(phrase_base)?))
}
