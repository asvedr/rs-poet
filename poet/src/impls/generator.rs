use crate::entities::errors::PoetError;
use crate::entities::phrase::Phrase;
use crate::entities::requests::{GetPhraseRequest, GetRhymeRequest};
use crate::entities::verse_schema::{RowSizeSchema, VerseSchema};
use crate::impls::randomizer::Randomizer;
use crate::proto::interface::IPhraseBase;
use crate::proto::poet::IGenerator;

pub struct Generator {
    phrase_base: Box<dyn IPhraseBase>,
    randomizer: Randomizer,
}

struct Context<'a> {
    schema: &'a VerseSchema,
    generated: Vec<Phrase>,
    sizes: Vec<usize>,
}

impl Generator {
    pub fn new(phrase_base: Box<dyn IPhraseBase>) -> Result<Self, PoetError> {
        Ok(Self {
            phrase_base,
            randomizer: Randomizer::new()?,
        })
    }

    fn try_generate_verse(&mut self, schema: &VerseSchema) -> Result<String, PoetError> {
        let mut context = Context {
            schema,
            generated: vec![],
            sizes: vec![],
        };
        for line_id in 0..schema.sizes.len() {
            let line = self.generate_line(&context, line_id)?;
            context.sizes.push(line.syl_count);
            context.generated.push(line);
        }
        Ok(Self::lines_to_string(context.generated))
    }

    fn generate_line(&mut self, ctx: &Context, line_id: usize) -> Result<Phrase, PoetError> {
        let size = self.deref_size(&ctx.sizes, &ctx.schema.sizes[line_id])?;
        match Self::find_rhyme_id_for_row(ctx, line_id) {
            None => self.gen_line_no_rhyme(ctx, size),
            Some(rhyme_id) if rhyme_id > line_id => {
                self.gen_line_rhyme_forward(ctx, size, rhyme_id)
            }
            Some(rhyme_id) if rhyme_id < line_id => {
                self.gen_line_rhyme_backward(ctx, size, rhyme_id)
            }
            _ => unreachable!(),
        }
    }

    fn deref_size(&mut self, sizes: &[usize], size: &RowSizeSchema) -> Result<usize, PoetError> {
        let val = match size {
            RowSizeSchema::Fixed(val) => *val,
            RowSizeSchema::Range { from, to } => self.randomizer.gen_range(*from, *to)?,
            RowSizeSchema::Relative {
                row_index,
                size_change,
            } => {
                if *row_index >= sizes.len() {
                    return Err(PoetError::InvalidSizeSchema);
                }
                let len = sizes[*row_index];
                ((len as isize) + size_change) as usize
            }
        };
        Ok(val)
    }

    fn gen_line_no_rhyme(&mut self, ctx: &Context, size: usize) -> Result<Phrase, PoetError> {
        let req = GetPhraseRequest::new()
            .syl_count(size)
            .exclude(&ctx.generated);
        Self::unwrap_opt_phrase(self.phrase_base.get_random_phrase(req)?)
    }

    fn gen_line_rhyme_forward(
        &mut self,
        ctx: &Context,
        size: usize,
        next_id: usize,
    ) -> Result<Phrase, PoetError> {
        let mut request = GetPhraseRequest::new()
            .exclude(&ctx.generated)
            .syl_count(size);
        let next_size = &ctx.schema.sizes[next_id];
        let mut sizes = ctx.sizes.clone();
        sizes.push(size);
        match self.deref_size(&sizes, next_size) {
            Ok(val) => request = request.new_rhyme_syl_count(val),
            Err(PoetError::InvalidSizeSchema) => (),
            Err(err) => return Err(err),
        }
        let res = self.phrase_base.get_random_phrase(request)?;
        Self::unwrap_opt_phrase(res)
    }

    fn gen_line_rhyme_backward(
        &mut self,
        ctx: &Context,
        size: usize,
        prev_id: usize,
    ) -> Result<Phrase, PoetError> {
        let prev_phrase = &ctx.generated[prev_id];
        let res = self.phrase_base.get_rhyme_to_phrase(
            prev_phrase,
            GetRhymeRequest {
                syl_count: Some(size),
                exclude: &ctx.generated,
            },
        )?;
        Self::unwrap_opt_phrase(res)
    }

    #[inline(always)]
    fn unwrap_opt_phrase(opt: Option<Phrase>) -> Result<Phrase, PoetError> {
        match opt {
            Some(val) => Ok(val),
            None => Err(PoetError::MatchedRowsNotFound),
        }
    }

    fn find_rhyme_id_for_row(ctx: &Context, line_id: usize) -> Option<usize> {
        for (a, b) in ctx.schema.rhymes.iter() {
            if *a == line_id {
                return Some(*b);
            } else if *b == line_id {
                return Some(*a);
            }
        }
        None
    }

    fn uppercase_first_symbol(text: String) -> String {
        if text.is_empty() {
            return text;
        }
        let mut chars = text.chars();
        let mut result = chars.next().unwrap().to_uppercase().to_string();
        for symbol in chars {
            result.push(symbol)
        }
        result
    }

    fn lines_to_string(lines: Vec<Phrase>) -> String {
        lines
            .into_iter()
            .map(|p| Self::uppercase_first_symbol(p.text))
            .collect::<Vec<_>>()
            .join("\n")
    }
}

impl IGenerator for Generator {
    fn generate_verse(
        &mut self,
        schema: &VerseSchema,
        max_tries: usize,
    ) -> Result<String, PoetError> {
        for _ in 0..max_tries.max(1) {
            match self.try_generate_verse(schema) {
                Ok(val) => return Ok(val),
                Err(PoetError::MatchedRowsNotFound) => (),
                Err(err) => return Err(err),
            }
        }
        Err(PoetError::MatchedRowsNotFound)
    }
}
