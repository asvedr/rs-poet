mod en;
mod ru;

use crate::entities::errors::PPResult;
use crate::proto::phrase_processor::{IPhraseMaker, IPhraseMakerFactory};
use crate::Lang;

macro_rules! match_lang {
    ($lang:expr, $method:ident, $($param:expr),*) => {
        match $lang {
            Lang::Ru => ru::Maker::$method($($param),*),
            Lang::En => en::Maker::$method($($param),*),
        }
    }
}

pub fn get_maker(lang: Lang, params: Vec<(String, String)>) -> PPResult<Box<dyn IPhraseMaker>> {
    match_lang!(lang, create, params)
}

pub fn get_maker_params(lang: Lang) -> Vec<(&'static str, &'static str)> {
    match_lang!(lang, params,)
}
