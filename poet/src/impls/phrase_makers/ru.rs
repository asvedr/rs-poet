use crate::entities::errors::{DbResult, PPResult, PhraseProcessorError};
use crate::entities::phrase::NewPhrase;
use crate::entities::word::Word;
use crate::impls::lang_utils::ru::RuUtils;
use crate::proto::phrase_processor::{IPhraseMaker, IPhraseMakerFactory};
use crate::{DbError, IWordBase};
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::Read;
use std::str::FromStr;

const BLACK_LIST_PARAM: &str = "bl";
const MIN_SYL_COUNT_PARAM: &str = "min_syl";
const MIN_SYL_COUNT_DEFAULT: usize = 1;
const MAX_SYL_COUNT_PARAM: &str = "max_syl";
const MAX_SYL_COUNT_DEFAULT: usize = 100;

pub struct Maker {
    lang: RuUtils,
    blacklist: HashSet<String>,
    min_syl_count: usize,
    max_syl_count: usize,
}

impl Maker {
    fn load_blacklist(path: &str) -> PPResult<HashSet<String>> {
        let mut file = File::open(path)?;
        let mut src = String::new();
        file.read_to_string(&mut src)?;
        let set = src
            .split('\n')
            .filter_map(|s| {
                let stripped = s.trim();
                if stripped.is_empty() {
                    return None;
                }
                Some(stripped.to_string())
            })
            .collect::<HashSet<_>>();
        Ok(set)
    }

    fn get_word(&self, word_base: &dyn IWordBase, src: &str) -> DbResult<Word> {
        if let Some(val) = self.lang.guess_word(src) {
            return Ok(val);
        }
        word_base.get_word(src)
    }

    fn contains_digits(text: &str) -> bool {
        for symbol in text.chars() {
            if symbol >= '0' && symbol <= '9' {
                return true
            }
        }
        false
    }
}

impl IPhraseMakerFactory for Maker {
    fn create(params: Vec<(String, String)>) -> PPResult<Box<dyn IPhraseMaker>> {
        let map = params.into_iter().collect::<HashMap<_, _>>();
        let blacklist = match map.get(BLACK_LIST_PARAM) {
            None => HashSet::new(),
            Some(path) => Self::load_blacklist(path)?,
        };
        let min_syl_count = match map.get(MIN_SYL_COUNT_PARAM) {
            Some(s) => {
                usize::from_str(s).map_err(|_| PhraseProcessorError::InvalidParam(s.clone()))?
            }
            _ => MIN_SYL_COUNT_DEFAULT,
        };
        let max_syl_count = match map.get(MAX_SYL_COUNT_PARAM) {
            Some(s) => {
                usize::from_str(s).map_err(|_| PhraseProcessorError::InvalidParam(s.clone()))?
            }
            _ => MAX_SYL_COUNT_DEFAULT,
        };
        let maker = Self {
            lang: RuUtils::new('`'),
            blacklist,
            min_syl_count,
            max_syl_count,
        };
        Ok(Box::new(maker))
    }

    fn params() -> Vec<(&'static str, &'static str)> {
        vec![
            (BLACK_LIST_PARAM, "last word blacklist path"),
            (MIN_SYL_COUNT_PARAM, "min phrase syl count(int)"),
            (MAX_SYL_COUNT_PARAM, "max phrase syl count(int)"),
        ]
    }
}

impl IPhraseMaker for Maker {
    fn make_phrase(&self, word_base: &dyn IWordBase, source: &str) -> PPResult<NewPhrase> {
        let mut last_word = String::new();
        let mut total_syl = 0;
        let mut slug = String::new();
        if Self::contains_digits(source) {
            return Err(PhraseProcessorError::Filtered)
        }
        for word in self.lang.split_phrase_to_words(source) {
            total_syl += self.lang.get_syl_count(&word);
            slug.push_str(&word);
            last_word = word;
        }
        if self.lang.get_syl_count(&last_word) == 0 {
            return Err(PhraseProcessorError::Filtered);
        }
        if total_syl < self.min_syl_count || total_syl > self.max_syl_count {
            return Err(PhraseProcessorError::Filtered);
        }
        let word = match self.get_word(word_base, &last_word) {
            Ok(val) => val,
            Err(DbError::NotFound) => return Err(PhraseProcessorError::UnknownWord(last_word)),
            Err(err) => return Err(err.into()),
        };
        if self.blacklist.contains(&last_word) {
            return Err(PhraseProcessorError::Filtered);
        }
        let phrase = NewPhrase {
            slug,
            text: source.to_string(),
            ending: word.ending,
            last_word,
            syl_count: total_syl,
        };
        Ok(phrase)
    }
}
