use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::Read;
use std::str::FromStr;

use crate::entities::errors::PPResult;
use crate::entities::phrase::NewPhrase;
use crate::entities::word::Word;
use crate::proto::phrase_processor::{IPhraseMaker, IPhraseMakerFactory};
use crate::utils::common::split_to_words;
use crate::{IWordBase, PhraseProcessorError};

const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyz'";
const BLACK_LIST_PARAM: &str = "bl";
const MIN_SYL_COUNT_PARAM: &str = "min_syl";
const MIN_SYL_COUNT_DEFAULT: usize = 1;
const MAX_SYL_COUNT_PARAM: &str = "max_syl";
const MAX_SYL_COUNT_DEFAULT: usize = 100;

pub struct Maker {
    alphabet: HashSet<char>,
    blacklist: HashSet<String>,
    min_syl_count: usize,
    max_syl_count: usize,
}

impl Maker {
    fn load_blacklist(path: &str) -> PPResult<HashSet<String>> {
        let mut file = File::open(path)?;
        let mut src = String::new();
        file.read_to_string(&mut src)?;
        let set = src
            .split('\n')
            .filter_map(|s| {
                let stripped = s.trim();
                if stripped.is_empty() {
                    return None;
                }
                Some(stripped.to_string())
            })
            .collect::<HashSet<_>>();
        Ok(set)
    }

    fn split_to_words(&self, src: &str) -> Vec<String> {
        let mut result = Vec::new();
        for word_vec in split_to_words(src, &self.alphabet) {
            let mut word_str = String::with_capacity(word_vec.len());
            for sym in word_vec {
                for l_sym in sym.to_lowercase() {
                    word_str.push(l_sym)
                }
            }
            result.push(word_str)
        }
        result
    }

    fn make_words_vec(words: &[Word], keys: Vec<String>) -> PPResult<Vec<&Word>> {
        let mut result = Vec::new();
        'outer: for key in keys {
            for word in words {
                if word.text == key {
                    result.push(word);
                    continue 'outer;
                }
            }
            let err = PhraseProcessorError::UnknownWord(key);
            return Err(err);
        }
        Ok(result)
    }
}

impl IPhraseMakerFactory for Maker {
    fn create(params: Vec<(String, String)>) -> PPResult<Box<dyn IPhraseMaker>> {
        let map = params.into_iter().collect::<HashMap<_, _>>();
        let blacklist = match map.get(BLACK_LIST_PARAM) {
            None => HashSet::new(),
            Some(path) => Self::load_blacklist(path)?,
        };
        let min_syl_count = match map.get(MIN_SYL_COUNT_PARAM) {
            Some(s) => {
                usize::from_str(s).map_err(|_| PhraseProcessorError::InvalidParam(s.clone()))?
            }
            _ => MIN_SYL_COUNT_DEFAULT,
        };
        let max_syl_count = match map.get(MAX_SYL_COUNT_PARAM) {
            Some(s) => {
                usize::from_str(s).map_err(|_| PhraseProcessorError::InvalidParam(s.clone()))?
            }
            _ => MAX_SYL_COUNT_DEFAULT,
        };
        let maker = Self {
            alphabet: ALPHABET.chars().collect(),
            blacklist,
            min_syl_count,
            max_syl_count,
        };
        Ok(Box::new(maker))
    }

    fn params() -> Vec<(&'static str, &'static str)> {
        vec![
            (BLACK_LIST_PARAM, "last word blacklist path"),
            (MIN_SYL_COUNT_PARAM, "min phrase syl count(int)"),
            (MAX_SYL_COUNT_PARAM, "max phrase syl count(int)"),
        ]
    }
}

impl IPhraseMaker for Maker {
    fn make_phrase(&self, word_base: &dyn IWordBase, source: &str) -> PPResult<NewPhrase> {
        let mut total_syl = 0;
        let mut slug = String::new();
        let words_src = self.split_to_words(source);
        let words_objs = word_base.get_words(&words_src)?;
        let mut last_word = "";
        let mut ending = "";
        let mut lw_syl = 0;
        for word in Self::make_words_vec(&words_objs, words_src)? {
            slug.push_str(&word.text);
            total_syl += word.syl_count;
            last_word = &word.text;
            ending = &word.ending;
            lw_syl = word.syl_count;
        }
        if lw_syl == 0 {
            return Err(PhraseProcessorError::Filtered);
        }
        if total_syl < self.min_syl_count || total_syl > self.max_syl_count {
            return Err(PhraseProcessorError::Filtered);
        }
        if self.blacklist.contains(last_word) {
            return Err(PhraseProcessorError::Filtered);
        }
        let phrase = NewPhrase {
            slug,
            text: source.to_string(),
            ending: ending.to_string(),
            last_word: last_word.to_string(),
            syl_count: total_syl,
        };
        Ok(phrase)
    }
}
