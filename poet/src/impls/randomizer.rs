use std::fs::File;
use std::io::Read;

use crate::entities::errors::RandomizerError;

const PATH: &str = "/dev/urandom";

pub struct Randomizer {
    source: File,
}

impl Randomizer {
    pub fn new() -> Result<Self, RandomizerError> {
        let source = File::open(PATH).map_err(|_| RandomizerError::CanNotOpenURandom)?;
        Ok(Self { source })
    }

    /// gen in u32 range
    #[inline]
    pub fn gen_int(&mut self) -> Result<usize, RandomizerError> {
        let mut buffer = 0_u32.to_le_bytes();
        self.source
            .read(&mut buffer)
            .map_err(|_| RandomizerError::CanNotReadURandom)?;
        Ok(u32::from_le_bytes(buffer) as usize)
    }

    pub fn gen_range(&mut self, from: usize, to: usize) -> Result<usize, RandomizerError> {
        let n = self.gen_int()?;
        Ok(from + (n % (to - from)))
    }
}
