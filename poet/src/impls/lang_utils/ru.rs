use crate::entities::word::Word;
use crate::utils::common::split_to_words;
use std::collections::{HashMap, HashSet};

const VOWELS: &str = "уеыаоэяиюё";
const ALWAYS_STRONG_VOWEL: char = 'ё';
const VOWELS_RHYME_MAP: &[(char, char)] =
    &[('ю', 'у'), ('е', 'э'), ('я', 'а'), ('ы', 'и'), ('ё', 'о')];
const CONSONANTS: &str = "бвгджзйклмнпрстфхцчшщъь";
const MAX_REPEAT_OF_SYMBOL: usize = 2;

pub struct RuUtils {
    vowels: HashSet<char>,
    vowels_rhyme_map: HashMap<char, char>,
    alphabet: HashSet<char>,
    strong_symbol: char,
}

struct StrongVowelNotFound {}

impl RuUtils {
    pub fn new(strong_symbol: char) -> Self {
        let vowels: HashSet<char> = VOWELS.chars().collect();
        let mut alphabet = vowels.clone();
        for sym in CONSONANTS.chars() {
            alphabet.insert(sym);
        }
        Self {
            strong_symbol,
            vowels,
            vowels_rhyme_map: VOWELS_RHYME_MAP.iter().cloned().collect(),
            alphabet,
        }
    }

    fn normalize_ending_vowels(&self, text: &[char]) -> Vec<char> {
        let mut result = Vec::new();
        for sym in text {
            let norm = match self.vowels_rhyme_map.get(sym) {
                None => *sym,
                Some(sym) => *sym,
            };
            result.push(norm)
        }
        result
    }

    fn get_vowel_indexes(&self, text: &[char]) -> Vec<usize> {
        let mut vowel_indexes = Vec::new();
        for (i, sym) in text.iter().enumerate() {
            if self.vowels.contains(sym) {
                vowel_indexes.push(i)
            }
        }
        vowel_indexes
    }

    fn calculate_ending(
        &self,
        text: &[char],
        vowel_indexes: &[usize],
        strong_symbol_index: Option<usize>,
    ) -> Result<Vec<char>, StrongVowelNotFound> {
        // For words without vowels
        let syl_count = vowel_indexes.len();
        if syl_count == 0 {
            return Ok(Vec::new());
        }
        let mut strong_vowel_index = None;
        // If strong vowel is MARKED
        if let Some(index) = strong_symbol_index {
            if index == 0 {
                panic!("Strong symbol index == 0. Strong symbol must be AFTER vowel, not before")
            }
            strong_vowel_index = Some(index - 1);
        }
        // This condition is for estimation on raw word
        if strong_vowel_index.is_none() {
            strong_vowel_index = Self::estimate_strong_vowel(text, vowel_indexes);
        }
        match strong_vowel_index {
            None => Err(StrongVowelNotFound {}),
            Some(index) => Ok(self.normalize_ending_vowels(&text[index..])),
        }
    }

    fn prepare_word(
        &self,
        text: &[char],
        strong_symbol_index: Option<usize>,
    ) -> Result<(usize, Vec<char>), StrongVowelNotFound> {
        let vowel_indexes = self.get_vowel_indexes(text);
        let syl_count = vowel_indexes.len();
        let ending = self.calculate_ending(text, &vowel_indexes, strong_symbol_index)?;
        Ok((syl_count, ending))
    }

    fn number_of_same_letter(symbol: char, word: &[char]) -> usize {
        for (i, sym) in word.iter().enumerate() {
            if symbol != *sym {
                return i;
            }
        }
        word.len()
    }

    fn reduce_word(word: &[char]) -> Vec<char> {
        // remove dublicate of symbols. Example: прииивет => привет
        let mut result = Vec::new();
        let mut i = 0;
        while i < word.len() {
            let symbol = word[i];
            let count = Self::number_of_same_letter(symbol, &word[i + 1..]);
            if count > MAX_REPEAT_OF_SYMBOL {
                result.push(symbol);
                i += count;
            } else {
                result.push(symbol);
                i += 1;
            }
        }
        result
    }

    fn estimate_strong_vowel(text: &[char], vowel_indexes: &[usize]) -> Option<usize> {
        if vowel_indexes.len() == 1 {
            return Some(vowel_indexes[0]);
        }
        for i in vowel_indexes {
            if text[*i] == ALWAYS_STRONG_VOWEL {
                return Some(*i);
            }
        }
        None
    }

    pub fn alphabet(&self) -> &HashSet<char> {
        &self.alphabet
    }

    pub fn get_syl_count(&self, word: &str) -> usize {
        let word = word.chars().collect::<Vec<_>>();
        self.get_vowel_indexes(&word).len()
    }

    pub fn get_syl_count_and_ending(&self, word: &str) -> Option<(usize, String)> {
        let mut word = word.chars().collect::<Vec<_>>();
        let mut strong_symbol_index = None;
        for (i, sym) in word.iter().enumerate() {
            if *sym == self.strong_symbol {
                strong_symbol_index = Some(i);
                break;
            }
        }
        if let Some(ref i) = strong_symbol_index {
            word.remove(*i);
        }
        let (syl_count, ending) = self.prepare_word(&word, strong_symbol_index).ok()?;
        let ending = ending.into_iter().collect();
        Some((syl_count, ending))
    }

    pub fn split_phrase_to_words(&self, phrase: &str) -> Vec<String> {
        let mut result = Vec::new();
        for word in split_to_words(phrase, &self.alphabet) {
            let word = Self::reduce_word(&word).into_iter().collect();
            result.push(word)
        }
        result
    }

    pub fn guess_word(&self, text: &str) -> Option<Word> {
        let mut last_vowel_index = 0;
        let mut vowel_count = 0;
        let as_vec = text.chars().collect::<Vec<_>>();
        for (index, symbol) in as_vec.iter().enumerate() {
            if self.vowels.contains(symbol) {
                vowel_count += 1;
                last_vowel_index = index;
            }
        }
        if vowel_count != 1 {
            return None;
        }
        let mut end = String::new();
        for symbol in &as_vec[last_vowel_index..] {
            let normalized = *self.vowels_rhyme_map.get(symbol).unwrap_or(symbol);
            end.push(normalized);
        }
        Some(Word {
            text: text.to_string(),
            syl_count: vowel_count,
            ending: end,
        })
    }
}
