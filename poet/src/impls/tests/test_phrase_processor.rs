use std::io::Write;
use tempfile::NamedTempFile;

use crate::entities::phrase::NewPhrase;
use crate::entities::word::Word;
use crate::{
    create_word_base, IPhraseProcessor, IWordBase, Lang, PhraseProcessor, PhraseProcessorError,
};

fn words_ru() -> Vec<Word> {
    vec![
        Word {
            text: "мгновенье".to_string(),
            syl_count: 3,
            ending: "эньэ".to_string(),
        },
        Word {
            text: "виденье".to_string(),
            syl_count: 3,
            ending: "эньэ".to_string(),
        },
        Word {
            text: "рука".to_string(),
            syl_count: 2,
            ending: "а".to_string(),
        },
    ]
}

fn words_en() -> Vec<Word> {
    vec![
        Word {
            text: "'cause".to_string(),
            syl_count: 1,
            ending: "AH0 Z".to_string(),
        },
        Word {
            text: "a".to_string(),
            syl_count: 1,
            ending: "AH0".to_string(),
        },
        Word {
            text: "a's".to_string(),
            syl_count: 1,
            ending: "EY1 Z".to_string(),
        },
        Word {
            text: "aaberg".to_string(),
            syl_count: 2,
            ending: "AA1 B ER0 G".to_string(),
        },
        Word {
            text: "aachen".to_string(),
            syl_count: 2,
            ending: "AA1 K AH0 N".to_string(),
        },
        Word {
            text: "aaker".to_string(),
            syl_count: 2,
            ending: "AA1 K ER0".to_string(),
        },
    ]
}

fn word_base_ru() -> Box<dyn IWordBase> {
    let mut base = create_word_base(":memory:", Lang::Ru, "hey").unwrap();
    base.add_words(&words_ru()).unwrap();
    base
}

fn word_base_en() -> Box<dyn IWordBase> {
    let mut base = create_word_base(":memory:", Lang::En, "hey").unwrap();
    base.add_words(&words_en()).unwrap();
    base
}

#[test]
fn test_ru_phrase_processor_get() {
    let base = word_base_ru();
    let processor = PhraseProcessor::new(base);
    assert_eq!(
        processor.get_params().unwrap(),
        &[
            ("bl", "last word blacklist path"),
            ("min_syl", "min phrase syl count(int)"),
            ("max_syl", "max phrase syl count(int)"),
        ]
    )
}

#[test]
fn test_ru_process_phrases() {
    let mut tf = NamedTempFile::new().unwrap();
    let phrases = [
        // ok
        "я помню мгновенье",
        // ok
        " Как виденье ",
        // ignored empty line
        "",
        // ignored empty line
        "  ",
        "#эта рука",
        // ignored last word
        "пошел к",
        // predicted without dict
        "большой дом",
        // bad word
        "это слово",
    ];
    for phrase in phrases {
        tf.write(format!("{}\n", phrase).as_bytes()).unwrap();
    }
    tf.flush().unwrap();
    let base = word_base_ru();
    let processor = PhraseProcessor::new(base);
    let collected = processor
        .load_file(tf.path(), vec![])
        .unwrap()
        .collect::<Vec<_>>();
    let expected: &[Result<NewPhrase, PhraseProcessorError>] = &[
        Ok(NewPhrase {
            slug: "япомнюмгновенье".to_string(),
            text: "я помню мгновенье".to_string(),
            ending: "эньэ".to_string(),
            last_word: "мгновенье".to_string(),
            syl_count: 6,
        }),
        Ok(NewPhrase {
            slug: "каквиденье".to_string(),
            text: "Как виденье".to_string(),
            ending: "эньэ".to_string(),
            last_word: "виденье".to_string(),
            syl_count: 4,
        }),
        Ok(NewPhrase {
            slug: "этарука".to_string(),
            text: "#эта рука".to_string(),
            ending: "а".to_string(),
            last_word: "рука".to_string(),
            syl_count: 4,
        }),
        Ok(NewPhrase {
            slug: "большойдом".to_string(),
            text: "большой дом".to_string(),
            ending: "ом".to_string(),
            last_word: "дом".to_string(),
            syl_count: 3,
        }),
        Err(PhraseProcessorError::UnknownWord("слово".to_string())),
    ];
    assert_eq!(collected, expected);
}

#[test]
fn test_en_processor_phrases() {
    let mut tf = NamedTempFile::new().unwrap();
    let phrases = [
        // ok
        "Aachen AabeRg",
        // unknown word "world"
        "world",
        // ignored empty line
        "",
        // unknown word "the"
        "aaker the",
        // ok
        "#aaker a ",
    ];
    for phrase in phrases {
        tf.write(format!("{}\n", phrase).as_bytes()).unwrap();
    }
    tf.flush().unwrap();
    let base = word_base_en();
    let processor = PhraseProcessor::new(base);
    let collected = processor
        .load_file(tf.path(), vec![])
        .unwrap()
        .collect::<Vec<_>>();
    let expected = &[
        Ok(NewPhrase {
            slug: "aachenaaberg".to_string(),
            text: "Aachen AabeRg".to_string(),
            ending: "AA1 B ER0 G".to_string(),
            last_word: "aaberg".to_string(),
            syl_count: 4,
        }),
        Err(PhraseProcessorError::UnknownWord("world".to_string())),
        Err(PhraseProcessorError::UnknownWord("the".to_string())),
        Ok(NewPhrase {
            slug: "aakera".to_string(),
            text: "#aaker a".to_string(),
            ending: "AH0".to_string(),
            last_word: "a".to_string(),
            syl_count: 3,
        }),
    ];
    assert_eq!(collected, expected)
}
