use rstest::rstest;
use std::cell::RefCell;

use crate::entities::db_settings::BaseSettings;
use crate::entities::errors::{DbResult, PoetError};
use crate::entities::phrase::{NewPhrase, Phrase};
use crate::entities::requests::{GetPhraseRequest, GetRhymeRequest};
use crate::entities::verse_schema::{RowSizeSchema, VerseSchema};
use crate::impls::generator::Generator;
use crate::proto::poet::IGenerator;
use crate::sqlite_repos::tests::test_rhymes::all_phrases;
use crate::{
    DbPhraseRepo, DbRhymeRepo, DbSettingRepo, IBaseLoader, IPhraseBase, Lang, PhraseBase,
    SQLConnection,
};

struct MockBase {
    resp_queue: RefCell<Vec<Option<Phrase>>>,
}

impl MockBase {
    fn new(resp_queue: Vec<Option<Phrase>>) -> Self {
        MockBase {
            resp_queue: RefCell::new(resp_queue),
        }
    }
}

impl IPhraseBase for MockBase {
    fn get_settings(&self) -> DbResult<BaseSettings> {
        todo!()
    }
    fn get_phrases_size(&self) -> DbResult<usize> {
        todo!()
    }
    fn get_rhymes_size(&self) -> DbResult<usize> {
        todo!()
    }
    fn add_phrases(&mut self, _: &[NewPhrase]) -> DbResult<()> {
        todo!()
    }
    fn get_by_slug(&self, _: &str) -> DbResult<Phrase> {
        todo!()
    }
    fn get_by_text(&self, _: &str) -> DbResult<Phrase> {
        todo!()
    }
    fn update_ending(&mut self, _: usize, _: &str) -> DbResult<()> {
        todo!()
    }
    fn update_last_word(&mut self, _: usize, _: &str) -> DbResult<()> {
        todo!()
    }
    fn update_syl(&mut self, _: usize, _: usize) -> DbResult<()> {
        todo!()
    }
    fn recalculate_rhymes(&mut self, _: usize, _: usize) -> DbResult<()> {
        todo!()
    }

    fn get_rhyme_to_phrase(&self, _: &Phrase, _: GetRhymeRequest) -> DbResult<Option<Phrase>> {
        Ok(self.resp_queue.borrow_mut().remove(0))
    }

    fn get_random_phrase(&self, _: GetPhraseRequest) -> DbResult<Option<Phrase>> {
        Ok(self.resp_queue.borrow_mut().remove(0))
    }
}

fn real_setup() -> Generator {
    let connect = SQLConnection::new(":memory:").unwrap();
    let mut base: PhraseBase<_, DbSettingRepo, DbPhraseRepo, DbRhymeRepo> =
        PhraseBase::create(connect, Lang::Ru, "").unwrap();
    base.add_phrases(&all_phrases()).unwrap();
    base.recalculate_rhymes(100, 5).unwrap();
    Generator::new(Box::new(base)).unwrap()
}

#[rstest]
fn test_gen_moment_fixed(
    #[values(
        RowSizeSchema::Fixed(9),
        RowSizeSchema::Relative {row_index: 0, size_change: 0}
    )]
    second_row_size: RowSizeSchema,
    #[values((0, 1), (1, 0))] rhyme: (usize, usize),
) {
    let mut generator = real_setup();
    let schema = VerseSchema {
        sizes: vec![RowSizeSchema::Fixed(9), second_row_size],
        rhymes: vec![rhyme],
    };
    let verse = generator.generate_verse(&schema, 0).unwrap();
    let line_a = "Я помню чудное мгновенье";
    let line_b = "Как мимолетное виденье";
    let verse_a = [line_a, line_b].join("\n");
    let verse_b = [line_b, line_a].join("\n");
    assert!(verse == verse_a || verse == verse_b);
}

#[rstest(
    max_retries,
    result,
    case(1, Err(PoetError::MatchedRowsNotFound)),
    case(2, Ok("Xxx\nYyy".to_string())),
)]
fn test_retries(max_retries: usize, result: Result<String, PoetError>) {
    let line1 = Phrase {
        id: 0,
        slug: "xxx".to_string(),
        text: "xxx".to_string(),
        ending: "xxx".to_string(),
        last_word: "xxx".to_string(),
        syl_count: 1,
    };
    let line2 = Phrase {
        id: 1,
        slug: "yyy".to_string(),
        text: "yyy".to_string(),
        ending: "yyy".to_string(),
        last_word: "yyy".to_string(),
        syl_count: 1,
    };
    let base = MockBase::new(vec![Some(line1.clone()), None, Some(line1), Some(line2)]);
    let mut gen = Generator::new(Box::new(base)).unwrap();
    let schema = VerseSchema {
        sizes: vec![RowSizeSchema::Fixed(1), RowSizeSchema::Fixed(2)],
        rhymes: vec![(0, 1)],
    };
    let verse = gen.generate_verse(&schema, max_retries);
    assert_eq!(verse, result)
}
