pub(crate) mod dict_loaders;
pub(crate) mod generator;
pub(crate) mod lang_utils;
pub(crate) mod phrase_base;
pub(crate) mod phrase_makers;
pub(crate) mod phrase_processor;
pub(crate) mod randomizer;
pub(crate) mod sqlite_repos;
#[cfg(test)]
mod tests;
pub(crate) mod universal_dict_loader;
pub(crate) mod word_base;
