use std::fs::File;
use std::path::Path;

use crate::entities::errors::{DLResult, DictLoaderError};
use crate::entities::fun_iterator::FunIterator;
use crate::entities::lang::Lang;
use crate::entities::phrase::NewPhrase;
use crate::entities::word::Word;
use crate::impls::dict_loaders::{phrase_loaders, word_loaders};
use crate::proto::dict::{ISpecificDictLoader, IUniversalDictLoader};

type BoxedLoader<T> = Box<dyn ISpecificDictLoader<T>>;
type LangMap<T> = Vec<(Lang, Vec<BoxedLoader<T>>)>;

pub struct UniversalDictLoader {
    word_loaders: LangMap<Word>,
    phrase_loaders: LangMap<NewPhrase>,
}

fn add_to_categorized<T>(loaders: &mut LangMap<T>, loader: BoxedLoader<T>) {
    let lang = loader.lang();
    for (key, val) in loaders.iter_mut() {
        if *key == lang {
            val.push(loader);
            return;
        }
    }
    loaders.push((lang, vec![loader]))
}

impl Default for UniversalDictLoader {
    fn default() -> Self {
        let mut w_categorized = Vec::new();
        let mut p_categorized = Vec::new();
        for loader in word_loaders() {
            add_to_categorized(&mut w_categorized, loader);
        }
        for loader in phrase_loaders() {
            add_to_categorized(&mut p_categorized, loader);
        }
        UniversalDictLoader {
            word_loaders: w_categorized,
            phrase_loaders: p_categorized,
        }
    }
}

impl UniversalDictLoader {
    fn get_loader_for_lang<'a, T>(
        loaders: &'a LangMap<T>,
        ext: &str,
        lang: Lang,
    ) -> DLResult<&'a dyn ISpecificDictLoader<T>> {
        let mut candidates = Vec::new();
        for (key, val) in loaders.iter() {
            if *key == lang {
                candidates.extend_from_slice(&Self::find_by_ext(val, ext))
            }
        }
        Self::choose(candidates, ext)
    }

    fn get_loader_for_all<'a, T>(
        loaders: &'a LangMap<T>,
        ext: &str,
    ) -> DLResult<&'a dyn ISpecificDictLoader<T>> {
        let mut candidates = Vec::new();
        for (_, val) in loaders.iter() {
            candidates.extend_from_slice(&Self::find_by_ext(val, ext))
        }
        Self::choose(candidates, ext)
    }

    fn find_by_ext<'a, T>(
        vec: &'a [Box<dyn ISpecificDictLoader<T>>],
        ext: &str,
    ) -> Vec<&'a dyn ISpecificDictLoader<T>> {
        vec.iter()
            .filter(|l| l.ext() == ext)
            .map(|l| &**l)
            .collect()
    }

    fn choose<'a, T>(
        vec: Vec<&'a dyn ISpecificDictLoader<T>>,
        ext: &str,
    ) -> DLResult<&'a dyn ISpecificDictLoader<T>> {
        let err = match vec.len() {
            1 => return Ok(vec[0]),
            0 => DictLoaderError::ExtProcessorNotFound(ext.to_string()),
            _ => DictLoaderError::TooManyProcessorsForExt(ext.to_string()),
        };
        Err(err)
    }

    fn load_file<T>(
        loaders: &LangMap<T>,
        path: &Path,
        lang: Option<Lang>,
    ) -> DLResult<FunIterator<DLResult<T>>> {
        let split = path
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .split('.')
            .collect::<Vec<_>>();
        if split.len() < 2 {
            let err = DictLoaderError::InvalidFilename(path.to_str().unwrap().to_string());
            return Err(err);
        }
        let ext = split[split.len() - 2];
        let loader = if let Some(lang) = lang {
            Self::get_loader_for_lang(loaders, ext, lang)
        } else {
            Self::get_loader_for_all(loaders, ext)
        }?;
        let file = File::open(path)?;
        Ok(loader.parse_file(file))
    }

    fn extensions<T>(loaders: &LangMap<T>) -> Vec<(Lang, Vec<&'static str>)> {
        loaders
            .iter()
            .map(|(lang, loaders)| {
                let exts = loaders.iter().map(|l| l.ext()).collect();
                (*lang, exts)
            })
            .collect()
    }
}

impl IUniversalDictLoader for UniversalDictLoader {
    fn load_words(&self, path: &Path, lang: Option<Lang>) -> DLResult<FunIterator<DLResult<Word>>> {
        Self::load_file(&self.word_loaders, path, lang)
    }

    fn load_phrases(
        &self,
        path: &Path,
        lang: Option<Lang>,
    ) -> DLResult<FunIterator<DLResult<NewPhrase>>> {
        Self::load_file(&self.phrase_loaders, path, lang)
    }

    fn extensions_words(&self) -> Vec<(Lang, Vec<&'static str>)> {
        Self::extensions(&self.word_loaders)
    }

    fn extensions_phrases(&self) -> Vec<(Lang, Vec<&'static str>)> {
        Self::extensions(&self.phrase_loaders)
    }
}
