use std::str::FromStr;

use crate::entities::db_settings::BaseSettings;
use crate::entities::errors::{DbError, DbResult};
use crate::impls::sqlite_repos::base::connection::SQLConnection;
use crate::impls::sqlite_repos::base::table_manager::TableManager;
use crate::impls::sqlite_repos::base::table_schema::{SStr, SVec, TableSchema};
use crate::impls::sqlite_repos::base::utils::{deescape, escape};
use crate::proto::db::{IDbRepo, INewDbRepo, ISettingsRepo};

pub struct DbSettingRepo {
    manager: TableManager<Table>,
}

struct Table {}

const C_KEY: &str = "key";
const C_VAL: &str = "value";

const BASE_TYPE: &str = "base_type";
const BASE_VERSION: &str = "base_version";
const POET_VERSION: &str = "poet_version";
const LANG: &str = "lang";
const DESCRIPTION: &str = "description";

impl TableSchema for Table {
    const NAME: SStr = "settings";
    const COLUMNS: SVec<(SStr, SStr)> = &[(C_KEY, "VARCHAR(50) NOT NULL"), (C_VAL, "TEXT")];
    const UNIQUE: SVec<SStr> = &[C_KEY];
}

impl DbSettingRepo {
    fn put_rows(&self, rows: Vec<(&str, String)>, replace: bool) -> DbResult<()> {
        let values = rows
            .into_iter()
            .map(|(k, v)| format!("({}, {})", escape(k), escape(&v)))
            .collect::<Vec<_>>()
            .join(", ");
        let option = if replace { "REPLACE" } else { "IGNORE" };
        let query = format!(
            r#"INSERT OR {} INTO {} ({}, {}) VALUES {}"#,
            option,
            Table::NAME,
            C_KEY,
            C_VAL,
            values,
        );
        self.manager.execute(&query)
    }

    fn get_rows(&self) -> DbResult<Vec<(String, String)>> {
        let query = format!("SELECT {}, {} FROM {}", C_KEY, C_VAL, Table::NAME,);
        self.manager
            .get_many(&query, |row| Ok((row.get(0)?, deescape(row.get(1)?))))
    }

    fn unwrap_row<T: FromStr>(rows: &[(String, String)], target: &str) -> DbResult<T> {
        for (key, val) in rows {
            if key == target {
                return match T::from_str(val) {
                    Ok(val) => Ok(val),
                    Err(_) => {
                        let msg = format!("{}.{}", Table::NAME, target);
                        Err(DbError::InvalidValueType(msg))
                    }
                };
            }
        }
        Err(DbError::RequiredValueNotFound(format!(
            "{}.{}",
            Table::NAME,
            target
        )))
    }

    fn make_rows(settings: BaseSettings) -> Vec<(&'static str, String)> {
        vec![
            (BASE_TYPE, settings.base_type.to_string()),
            (BASE_VERSION, settings.base_version),
            (POET_VERSION, settings.poet_version),
            (LANG, settings.lang.to_string()),
            (DESCRIPTION, settings.description),
        ]
    }
}

impl INewDbRepo<SQLConnection> for DbSettingRepo {
    fn create(connect: SQLConnection) -> Self {
        Self {
            manager: TableManager::<Table>::create(connect),
        }
    }
}

impl IDbRepo for DbSettingRepo {
    fn as_super(&self) -> &dyn IDbRepo {
        &self.manager
    }
}

impl ISettingsRepo<SQLConnection> for DbSettingRepo {
    fn init_defaults(&self, settings: BaseSettings) -> DbResult<()> {
        self.put_rows(Self::make_rows(settings), false)
    }
    fn set_settings(&self, settings: BaseSettings) -> DbResult<()> {
        self.put_rows(Self::make_rows(settings), true)
    }
    fn get_settings(&self) -> DbResult<BaseSettings> {
        let rows = self.get_rows()?;
        let settings = BaseSettings {
            base_type: Self::unwrap_row(&rows, BASE_TYPE)?,
            base_version: Self::unwrap_row(&rows, BASE_VERSION)?,
            poet_version: Self::unwrap_row(&rows, POET_VERSION)?,
            lang: Self::unwrap_row(&rows, LANG)?,
            description: Self::unwrap_row(&rows, DESCRIPTION)?,
        };
        Ok(settings)
    }
}
