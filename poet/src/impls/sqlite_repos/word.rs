use std::fmt::Write;

use crate::entities::errors::DbResult;
use crate::entities::word::Word;
use crate::impls::sqlite_repos::base::connection::SQLConnection;
use crate::impls::sqlite_repos::base::table_manager::TableManager;
use crate::impls::sqlite_repos::base::table_schema::{SStr, SVec, TableSchema};
use crate::impls::sqlite_repos::base::utils::{deescape, escape};
use crate::proto::db::{IDbRepo, INewDbRepo, IWordRepo};

pub struct DbWordRepo {
    manager: TableManager<Table>,
}

struct Table {}

const C_TEXT: &str = "text";
const C_SYL: &str = "syl";
const C_ENDING: &str = "ending";

impl TableSchema for Table {
    const NAME: SStr = "word";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        (C_TEXT, "TEXT PRIMARY KEY"),
        (C_SYL, "INTEGER NOT NULL"),
        (C_ENDING, "TEXT NOT NULL"),
    ];
    const INDEXES: SVec<SStr> = &[C_TEXT];
}

impl INewDbRepo<SQLConnection> for DbWordRepo {
    fn create(connect: SQLConnection) -> Self {
        Self {
            manager: TableManager::create(connect),
        }
    }
}

impl IDbRepo for DbWordRepo {
    fn as_super(&self) -> &dyn IDbRepo {
        &self.manager
    }
}

fn deserialize_word(row: &rusqlite::Row) -> rusqlite::Result<Word> {
    Ok(Word {
        text: deescape(row.get(0)?),
        syl_count: row.get(1)?,
        ending: deescape(row.get(2)?),
    })
}

impl IWordRepo<SQLConnection> for DbWordRepo {
    fn add_words(&self, words: &[Word]) -> DbResult<()> {
        if words.is_empty() {
            return Ok(());
        }
        let first = &words[0];
        let mut query = format!(
            "INSERT OR IGNORE INTO {} ({}, {}, {}) VALUES ({}, {}, {})",
            Table::NAME,
            C_TEXT,
            C_SYL,
            C_ENDING,
            escape(&first.text),
            first.syl_count,
            escape(&first.ending),
        );
        for word in words.iter().skip(1) {
            let _ = write!(
                query,
                ", ({}, {}, {})",
                escape(&word.text),
                word.syl_count,
                escape(&word.ending),
            );
        }
        self.manager.execute(&query)
    }

    fn get_word(&self, text: &str) -> DbResult<Word> {
        let query = format!(
            "SELECT {}, {}, {} FROM {} WHERE text = {}",
            C_TEXT,
            C_SYL,
            C_ENDING,
            Table::NAME,
            escape(text)
        );
        self.manager.get_one(&query, deserialize_word)
    }

    fn get_words(&self, text: &[String]) -> DbResult<Vec<Word>> {
        let value = text
            .iter()
            .map(|s| escape(s))
            .collect::<Vec<_>>()
            .join(", ");
        let query = format!(
            "SELECT {}, {}, {} FROM {} WHERE text IN ({})",
            C_TEXT,
            C_SYL,
            C_ENDING,
            Table::NAME,
            value
        );
        self.manager.get_many(&query, deserialize_word)
    }

    fn update_ending(&self, text: &str, new_end: &str) -> DbResult<()> {
        let query = format!(
            "UPDATE {} SET {} = {} WHERE {} = {}",
            Table::NAME,
            C_ENDING,
            escape(new_end),
            C_TEXT,
            escape(text),
        );
        self.manager.execute(&query)
    }

    fn update_syl(&self, text: &str, new_syl: usize) -> DbResult<()> {
        let query = format!(
            "UPDATE {} SET {} = {} WHERE {} = {}",
            Table::NAME,
            C_SYL,
            new_syl,
            C_TEXT,
            escape(text),
        );
        self.manager.execute(&query)
    }
}
