use std::fmt::Write;

use crate::entities::errors::{DbError, DbResult};
use crate::entities::phrase::Phrase;
use crate::entities::requests::{GetPhraseRequest, GetRhymeRequest};
use crate::impls::sqlite_repos::base::connection::SQLConnection;
use crate::impls::sqlite_repos::base::table_manager::TableManager;
use crate::impls::sqlite_repos::base::table_schema::{SStr, SVec, TableSchema};
use crate::impls::sqlite_repos::phrase::Table as PTable;
use crate::impls::sqlite_repos::phrase::{DbPhraseRepo, C_ENDING, C_LAST_WORD, C_SYL};
use crate::proto::db::{IDbRepo, INewDbRepo, IPhraseRepo, IRhymeRepo};

pub struct DbRhymeRepo {
    manager: TableManager<Table>,
    phrases: DbPhraseRepo,
}

struct Table {}

const C_ID: &str = "id";
const C_PHRASE_FROM_ID: &str = "phrase_f_id";
const C_PHRASE_TO_ID: &str = "phrase_t_id";
const C_PHRASE_FROM_SYL: &str = "f_syl";
const C_PHRASE_TO_SYL: &str = "t_syl";

impl TableSchema for Table {
    const NAME: SStr = "rhyme";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        (C_ID, "INTEGER PRIMARY KEY"),
        (C_PHRASE_FROM_ID, "INTEGER NOT NULL"),
        (C_PHRASE_TO_ID, "INTEGER NOT NULL"),
        (C_PHRASE_FROM_SYL, "INTEGER NOT NULL"),
        (C_PHRASE_TO_SYL, "INTEGER NOT NULL"),
    ];
    const INDEXES: SVec<SStr> = &[
        C_PHRASE_FROM_ID,
        C_PHRASE_TO_ID,
        C_PHRASE_FROM_SYL,
        C_PHRASE_TO_SYL,
    ];
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> = &[
        (C_PHRASE_FROM_ID, PTable::NAME, PTable::PK),
        (C_PHRASE_TO_ID, PTable::NAME, PTable::PK),
    ];
}

impl DbRhymeRepo {
    #[cfg(test)]
    pub fn get_all_rhymes_to(&self, phrase: &Phrase) -> DbResult<Vec<(usize, usize)>> {
        let query = format!(
            "SELECT `{to_id}`, `{to_syl}` FROM `{table}` WHERE `{from_id}` = {val}",
            to_id = C_PHRASE_TO_ID,
            to_syl = C_PHRASE_TO_SYL,
            table = Table::NAME,
            from_id = C_PHRASE_FROM_ID,
            val = phrase.id,
        );
        self.manager
            .get_many::<(usize, usize), _>(&query, |row| Ok((row.get(0)?, row.get(1)?)))
    }

    fn phrases_conditions(syl: Option<usize>, exclude: &[Phrase]) -> Vec<String> {
        let mut result = Vec::new();
        if let Some(syl) = syl {
            result.push(format!("(`{}` = {})", C_PHRASE_FROM_SYL, syl))
        }
        if !exclude.is_empty() {
            let cond = format!(
                "(`{}` NOT IN ({}))",
                C_PHRASE_FROM_ID,
                exclude
                    .iter()
                    .map(|p| p.id.to_string())
                    .collect::<Vec<_>>()
                    .join(", ")
            );
            result.push(cond)
        }
        result
    }

    fn rhymes_conditions(syl: Option<usize>, exclude: &[Phrase]) -> Vec<String> {
        let mut result = Vec::new();
        if let Some(syl) = syl {
            let cond = format!("(`{}` = {})", C_PHRASE_TO_SYL, syl,);
            result.push(cond);
        }
        if !exclude.is_empty() {
            let cond = format!(
                "(`{}` NOT IN ({}))",
                C_PHRASE_TO_ID,
                exclude
                    .iter()
                    .map(|p| p.id.to_string())
                    .collect::<Vec<_>>()
                    .join(", ")
            );
            result.push(cond);
        }
        result
    }

    fn random_phrase_query(
        table: &str,
        phrase_source_syl: Option<usize>,
        phrase_rhyme_syl: Option<usize>,
        exclude: &[Phrase],
        exclude_rhymes: &[Phrase],
    ) -> String {
        let mut conditions = Self::phrases_conditions(phrase_source_syl, exclude);
        conditions.append(&mut Self::rhymes_conditions(
            phrase_rhyme_syl,
            exclude_rhymes,
        ));
        let conditions = conditions.join(" AND ");
        let mut query = format!(
            "SELECT DISTINCT `{phrase_id}` FROM `{rhymes}` ",
            phrase_id = C_PHRASE_FROM_ID,
            rhymes = table,
        );
        if !conditions.is_empty() {
            let _ = write!(query, "WHERE {} ", conditions);
        }
        query.push_str("ORDER BY RANDOM() LIMIT 1");
        query
    }

    fn make_recalculation_query(min_id: usize, max_id: usize, max_syl_diff: usize) -> String {
        format!(
            r#"with source as (
                   SELECT `{id}`, `{ending}`, `{lw}`, `{syl}`
                   FROM `{t_ph}` WHERE `{id}` >= {min_id} AND `{id}` <= {max_id}
            ), variants as (
                SELECT
                       source.`{id}` as `{from_id}`,
                       source.`{syl}` as `{from_syl}`,
                       `{t_ph}`.`{id}` as `{to_id}`,
                       `{t_ph}`.`{syl}` as `{to_syl}`
                FROM source JOIN `{t_ph}` ON source.`{ending}` = `{t_ph}`.`{ending}`
                WHERE
                    source.`{lw}` <> `{t_ph}`.`{lw}`
                    AND ABS(source.`{syl}` - `{t_ph}`.`{syl}`) <= {max_syl_diff}
            ) INSERT INTO `{t_rh}`
                (`{from_id}`, `{from_syl}`, `{to_id}`, `{to_syl}`)
            SELECT * FROM variants"#,
            id = PTable::PK,
            ending = C_ENDING,
            lw = C_LAST_WORD,
            syl = C_SYL,
            t_ph = PTable::NAME,
            from_id = C_PHRASE_FROM_ID,
            from_syl = C_PHRASE_FROM_SYL,
            to_id = C_PHRASE_TO_ID,
            to_syl = C_PHRASE_TO_SYL,
            t_rh = Table::NAME,
            min_id = min_id,
            max_id = max_id,
            max_syl_diff = max_syl_diff,
        )
    }
}

impl INewDbRepo<SQLConnection> for DbRhymeRepo {
    fn create(connect: SQLConnection) -> Self {
        Self {
            manager: TableManager::create(connect.clone()),
            phrases: DbPhraseRepo::create(connect),
        }
    }
}

impl IDbRepo for DbRhymeRepo {
    fn as_super(&self) -> &dyn IDbRepo {
        &self.manager
    }

    fn init(&self) -> DbResult<()> {
        self.phrases.init()?;
        self.manager.init()
    }
}

fn deserialize_pk(row: &rusqlite::Row) -> rusqlite::Result<usize> {
    row.get(0)
}

impl IRhymeRepo<SQLConnection> for DbRhymeRepo {
    fn recalculate_rhymes(&self, batch_size: usize, max_syl_diff: usize) -> DbResult<()> {
        self.recreate_table()?;
        self.drop_indexes()?;
        let mut last_pk = 0;
        loop {
            let (min_id, max_id) = match self.phrases.get_id_range(last_pk, batch_size)? {
                None => break,
                Some(val) => val,
            };
            last_pk = max_id;
            let query = Self::make_recalculation_query(min_id, max_id, max_syl_diff);
            self.manager.execute(&query)?;
        }
        self.set_indexes()
    }

    fn get_rhyme_to_phrase(
        &self,
        phrase: &Phrase,
        request: GetRhymeRequest,
    ) -> DbResult<Option<Phrase>> {
        let mut exclude = request.exclude.iter().collect::<Vec<&Phrase>>();
        exclude.push(phrase);
        let phrases_subset_query = format!(
            r#"SELECT * FROM `{rhymes}` WHERE `{phrase_pk}` = {phrase_id}"#,
            rhymes = Table::NAME,
            phrase_pk = C_PHRASE_TO_ID,
            phrase_id = phrase.id,
        );
        let cte_name = "filtered_rhymes";
        let mut phrase_id_query = format!("WITH {} as ({}) ", cte_name, phrases_subset_query);
        phrase_id_query.push_str(&Self::random_phrase_query(
            cte_name,
            request.syl_count,
            None,
            request.exclude,
            &[],
        ));
        let id = match self.manager.get_one(&phrase_id_query, deserialize_pk) {
            Ok(val) => val,
            Err(DbError::NotFound) => return Ok(None),
            Err(err) => return Err(err),
        };
        let phrase = self.phrases.get_by_id(id)?;
        Ok(Some(phrase))
    }

    fn get_random_phrase(&self, request: GetPhraseRequest) -> DbResult<Option<Phrase>> {
        let query = Self::random_phrase_query(
            Table::NAME,
            request.syl_count,
            request.rhyme_syl_count,
            request.exclude,
            request.exclude_rhyme,
        );
        let id = match self.manager.get_one(&query, deserialize_pk) {
            Ok(val) => val,
            Err(DbError::NotFound) => return Ok(None),
            Err(err) => return Err(err),
        };
        let phrase = self.phrases.get_by_id(id)?;
        Ok(Some(phrase))
    }
}
