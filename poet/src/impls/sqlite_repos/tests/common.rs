use crate::impls::sqlite_repos::base::connection::SQLConnection;

pub fn mem_connection() -> SQLConnection {
    SQLConnection::new(":memory:").unwrap()
}
