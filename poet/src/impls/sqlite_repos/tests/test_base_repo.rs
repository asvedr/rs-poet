use crate::impls::sqlite_repos::base::table_manager::TableManager;
use crate::impls::sqlite_repos::base::table_schema::{SStr, SVec, TableSchema};
use crate::impls::sqlite_repos::tests::common::mem_connection;
use crate::proto::db::{IDbRepo, INewDbRepo};

struct Human {}
struct Car {}

impl TableSchema for Human {
    const NAME: SStr = "human";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER NOT NULL PRIMARY KEY"),
        ("name", "TEXT NOT NULL"),
        ("age", "INTEGER NOT NULL"),
    ];
}

impl TableSchema for Car {
    const NAME: SStr = "car";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER NOT NULL PRIMARY KEY"),
        ("number", "INTEGER NOT NULL"),
        ("owner", "INTEGER NOT NULL"),
    ];
    const INDEXES: SVec<SStr> = &["number", "owner"];
    const UNIQUE: SVec<SStr> = &["number"];
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> = &[("owner", "human", "id")];
}

#[test]
fn test_human_repo() {
    let cnn = mem_connection();
    let man = TableManager::<Human>::create(cnn);
    man.init().unwrap();
    assert_eq!(man.get_size().unwrap(), 0);
}

#[test]
fn test_both_repo() {
    let cnn = mem_connection();
    let hu_man = TableManager::<Human>::create(cnn.clone());
    let car_man = TableManager::<Car>::create(cnn.clone());
    hu_man.init().unwrap();
    car_man.init().unwrap();
    cnn.get()
        .execute("INSERT INTO human (name, age) VALUES ('Alex', 20)", ())
        .unwrap();
    let id = cnn.get().last_insert_rowid();
    cnn.get()
        .execute("INSERT INTO car (number, owner) VALUES (123, ?)", [id])
        .unwrap();
    cnn.get()
        .execute("INSERT INTO car (number, owner) VALUES (321, ?)", [id])
        .unwrap();
    let res = cnn
        .get()
        .execute("INSERT INTO car (number, owner) VALUES (123, ?)", [id]);
    assert!(res.is_err());
    let rows = cnn
        .get()
        .prepare(
            r#"
        SELECT human.name, car.number
        FROM human JOIN car ON human.id = car.owner
        ORDER BY car.number
    "#,
        )
        .unwrap()
        .query_map((), |row| {
            Ok((row.get::<_, String>(0)?, row.get::<_, usize>(1)?))
        })
        .unwrap()
        .collect::<Result<Vec<_>, _>>()
        .unwrap();
    assert_eq!(rows, [("Alex".to_string(), 123), ("Alex".to_string(), 321)])
}
