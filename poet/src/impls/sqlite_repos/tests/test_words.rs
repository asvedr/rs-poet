use crate::entities::errors::DbError;
use crate::entities::word::Word;
use crate::impls::sqlite_repos::tests::common::mem_connection;
use crate::impls::sqlite_repos::word::DbWordRepo;
use crate::proto::db::{IDbRepo, INewDbRepo, IWordRepo};

#[test]
fn test_add_get_words() {
    let repo = DbWordRepo::create(mem_connection());
    repo.init().unwrap();
    let words = &[
        Word {
            text: "повар".to_string(),
            syl_count: 2,
            ending: "овар".to_string(),
        },
        Word {
            text: "beebaa".to_string(),
            syl_count: 2,
            ending: "baa".to_string(),
        },
    ];
    repo.add_words(words).unwrap();
    assert_eq!(repo.get_size(), Ok(2));
    assert_eq!(repo.get_word(&words[0].text), Ok(words[0].clone()));
    assert_eq!(repo.get_word(&words[1].text), Ok(words[1].clone()));
    assert_eq!(repo.get_word("kek"), Err(DbError::NotFound))
}

#[test]
fn insert_ignore() {
    let repo = DbWordRepo::create(mem_connection());
    repo.init().unwrap();
    let words = &[
        Word {
            text: "повар".to_string(),
            syl_count: 2,
            ending: "овар".to_string(),
        },
        Word {
            text: "повар".to_string(),
            syl_count: 2,
            ending: "ивар".to_string(),
        },
    ];
    repo.add_words(words).unwrap();
    assert_eq!(repo.get_size(), Ok(1));
    assert_eq!(repo.get_word("повар").unwrap().ending, "овар");
}

#[test]
fn change_end() {
    let repo = DbWordRepo::create(mem_connection());
    repo.init().unwrap();
    let words = &[Word {
        text: "xxx".to_string(),
        syl_count: 2,
        ending: "eee".to_string(),
    }];
    repo.add_words(words).unwrap();
    assert_eq!(repo.get_word("xxx").unwrap().ending, "eee");
    repo.update_ending("xxx", "rrr").unwrap();
    assert_eq!(repo.get_word("xxx").unwrap().ending, "rrr");
}
