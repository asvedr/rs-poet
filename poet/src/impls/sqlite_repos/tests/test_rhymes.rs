use std::collections::HashSet;

use crate::entities::phrase::NewPhrase;
use crate::entities::requests::{GetPhraseRequest, GetRhymeRequest};
use crate::impls::sqlite_repos::phrase::DbPhraseRepo;
use crate::impls::sqlite_repos::rhyme::DbRhymeRepo;
use crate::impls::sqlite_repos::tests::common::mem_connection;
use crate::proto::db::{IDbRepo, INewDbRepo, IPhraseRepo, IRhymeRepo};

pub fn all_phrases() -> Vec<NewPhrase> {
    vec![
        NewPhrase {
            slug: "япомнючудноемгновенье".to_string(),
            text: "я помню чудное мгновенье".to_string(),
            ending: "эньэ".to_string(),
            last_word: "мгновенье".to_string(),
            syl_count: 9,
        },
        NewPhrase {
            slug: "какмимолетноевиденье".to_string(),
            text: "как мимолетное виденье".to_string(),
            ending: "эньэ".to_string(),
            last_word: "виденье".to_string(),
            syl_count: 9,
        },
        NewPhrase {
            slug: "ногамитопал".to_string(),
            text: "ногами топал".to_string(),
            ending: "опал".to_string(),
            last_word: "топал".to_string(),
            syl_count: 5,
        },
        NewPhrase {
            slug: "дверьюхлопал".to_string(),
            text: "дверью хлопал".to_string(),
            ending: "опал".to_string(),
            last_word: "хлопал".to_string(),
            syl_count: 4,
        },
        NewPhrase {
            slug: "рукамихлопал".to_string(),
            text: "руками хлопал".to_string(),
            ending: "опал".to_string(),
            last_word: "хлопал".to_string(),
            syl_count: 5,
        },
        NewPhrase {
            slug: "выпаласыпь".to_string(),
            text: "выпала сыпь".to_string(),
            ending: "ипь".to_string(),
            last_word: "сыпь".to_string(),
            syl_count: 4,
        },
        NewPhrase {
            slug: "полесуходил".to_string(),
            text: "по лесу ходил".to_string(),
            ending: "ил".to_string(),
            last_word: "ходил".to_string(),
            syl_count: 5,
        },
        NewPhrase {
            slug: "кеки".to_string(),
            text: "кеки".to_string(),
            ending: "эки".to_string(),
            last_word: "кеки".to_string(),
            syl_count: 2,
        },
        NewPhrase {
            slug: "закопал".to_string(),
            text: "закопал".to_string(),
            ending: "опал".to_string(),
            last_word: "закопал".to_string(),
            syl_count: 3,
        },
    ]
}

const RHYMED_PHRASES: &[&str] = &[
    "я помню чудное мгновенье",
    "как мимолетное виденье",
    "ногами топал",
    "дверью хлопал",
    "руками хлопал",
    "закопал",
];

fn setup() -> (DbPhraseRepo, DbRhymeRepo) {
    let connect = mem_connection();
    let phrase_repo = DbPhraseRepo::create(connect.clone());
    let rhyme_repo = DbRhymeRepo::create(connect);
    rhyme_repo.init().unwrap();
    phrase_repo.add_phrases(&all_phrases()).unwrap();
    rhyme_repo.recalculate_rhymes(100, 2).unwrap();
    (phrase_repo, rhyme_repo)
}

#[test]
fn test_empty_base() {
    let rhyme_repo = DbRhymeRepo::create(mem_connection());
    rhyme_repo.init().unwrap();
    assert_eq!(rhyme_repo.get_size(), Ok(0));
    rhyme_repo.recalculate_rhymes(100, 2).unwrap();
    assert_eq!(rhyme_repo.get_size(), Ok(0));
}

#[test]
fn test_recalculate_rhymes_moment() {
    let (phrase_repo, rhyme_repo) = setup();
    let phrase_a = phrase_repo.get_by_text("я помню чудное мгновенье").unwrap();
    let phrase_b = phrase_repo.get_by_text("как мимолетное виденье").unwrap();
    let rhymes_to_a = rhyme_repo.get_all_rhymes_to(&phrase_a).unwrap();
    assert_eq!(rhymes_to_a, &[(phrase_b.id, phrase_b.syl_count)]);
    let rhymes_to_b = rhyme_repo.get_all_rhymes_to(&phrase_b).unwrap();
    assert_eq!(rhymes_to_b, &[(phrase_a.id, phrase_a.syl_count)]);
}

#[test]
fn test_recalculate_rhymes_opal() {
    let (phrase_repo, rhyme_repo) = setup();
    let phrase_a = phrase_repo.get_by_text("ногами топал").unwrap();
    let phrase_b = phrase_repo.get_by_text("дверью хлопал").unwrap();
    let phrase_c = phrase_repo.get_by_text("руками хлопал").unwrap();
    let phrase_d = phrase_repo.get_by_text("закопал").unwrap();

    let mut values = rhyme_repo.get_all_rhymes_to(&phrase_a).unwrap();
    values.sort_by_key(|(val, _)| val.clone());
    let expected = vec![
        (phrase_b.id, phrase_b.syl_count),
        (phrase_c.id, phrase_c.syl_count),
        (phrase_d.id, phrase_d.syl_count),
    ];
    assert_eq!(values, expected);

    let mut values = rhyme_repo.get_all_rhymes_to(&phrase_b).unwrap();
    values.sort_by_key(|(val, _)| val.clone());
    let expected = vec![
        (phrase_a.id, phrase_a.syl_count),
        (phrase_d.id, phrase_d.syl_count),
    ];
    assert_eq!(values, expected);

    let mut values = rhyme_repo.get_all_rhymes_to(&phrase_c).unwrap();
    values.sort_by_key(|(val, _)| val.clone());
    let expected = vec![
        (phrase_a.id, phrase_a.syl_count),
        (phrase_d.id, phrase_d.syl_count),
    ];
    assert_eq!(values, expected);
}

#[test]
fn test_recalculate_rhymes_opal_max_diff() {
    let (phrase_repo, rhyme_repo) = setup();
    let phrase_a = phrase_repo.get_by_text("ногами топал").unwrap();
    let phrase_b = phrase_repo.get_by_text("дверью хлопал").unwrap();
    let phrase_c = phrase_repo.get_by_text("руками хлопал").unwrap();

    rhyme_repo.recalculate_rhymes(100, 1).unwrap();
    let mut values = rhyme_repo.get_all_rhymes_to(&phrase_a).unwrap();
    values.sort_by_key(|(val, _)| val.clone());
    let expected = vec![
        (phrase_b.id, phrase_b.syl_count),
        (phrase_c.id, phrase_c.syl_count),
    ];
    assert_eq!(values, expected);
}

#[test]
fn test_get_rhymes_to_unrhymed() {
    let (phrase_repo, rhyme_repo) = setup();
    let phrase = phrase_repo.get_by_text("выпала сыпь").unwrap();
    assert!(rhyme_repo.get_all_rhymes_to(&phrase).unwrap().is_empty());
}

#[test]
fn test_get_random_rhymed() {
    let (_, rhyme_repo) = setup();
    let mut unused_keys = RHYMED_PHRASES
        .into_iter()
        .map(|s| *s)
        .collect::<HashSet<&str>>();
    let mut used = Vec::new();
    for _ in RHYMED_PHRASES {
        let opt_phrase = rhyme_repo
            .get_random_phrase(GetPhraseRequest::new().exclude(&used))
            .unwrap();
        let phrase = opt_phrase.unwrap();
        assert!(!used.contains(&phrase));
        assert!(unused_keys.contains(&phrase.text as &str));
        unused_keys.remove(&phrase.text as &str);
        used.push(phrase);
    }
    assert!(unused_keys.is_empty());
    let opt_phrase = rhyme_repo
        .get_random_phrase(GetPhraseRequest::new().exclude(&used))
        .unwrap();
    assert!(opt_phrase.is_none());
}

#[test]
fn test_get_random_moment() {
    let (_, rhyme_repo) = setup();
    let opt_phrase = rhyme_repo
        .get_random_phrase(GetPhraseRequest::new().syl_count(9).new_rhyme_syl_count(9))
        .unwrap();
    assert!(opt_phrase.is_some());
    let phrase = opt_phrase.unwrap();
    assert!(phrase.slug == "япомнючудноемгновенье" || phrase.slug == "какмимолетноевиденье");
}

#[test]
fn test_get_random_rhymed_predict() {
    let (_, rhyme_repo) = setup();
    let opt_phrase = rhyme_repo
        .get_random_phrase(GetPhraseRequest::new().syl_count(4).new_rhyme_syl_count(3))
        .unwrap();
    let phrase = opt_phrase.unwrap();
    assert_eq!(phrase.text, "дверью хлопал");
    let opt_phrase = rhyme_repo
        .get_random_phrase(GetPhraseRequest::new().syl_count(16))
        .unwrap();
    assert!(opt_phrase.is_none());
}

#[test]
fn test_get_rhyme_door() {
    let (phrase_repo, rhyme_repo) = setup();
    let phrase = phrase_repo.get_by_text("дверью хлопал").unwrap();
    let opt_rhyme = rhyme_repo
        .get_rhyme_to_phrase(&phrase, GetRhymeRequest::new().syl_count(3))
        .unwrap();
    let rhyme = opt_rhyme.unwrap();
    assert_eq!(rhyme.text, "закопал");
}

#[test]
fn test_get_rhyme_moment() {
    let (phrase_repo, rhyme_repo) = setup();
    let phrase = phrase_repo.get_by_text("я помню чудное мгновенье").unwrap();
    let opt_rhyme = rhyme_repo
        .get_rhyme_to_phrase(&phrase, GetRhymeRequest::new())
        .unwrap();
    let rhyme = opt_rhyme.unwrap();
    assert_eq!(rhyme.text, "как мимолетное виденье");
    let opt_rhyme = rhyme_repo
        .get_rhyme_to_phrase(&phrase, GetRhymeRequest::new().exclude(&[rhyme]))
        .unwrap();
    assert!(opt_rhyme.is_none());
}
