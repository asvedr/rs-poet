use crate::entities::db_settings::{BaseSettings, BaseType};
use crate::entities::errors::DbError;
use crate::entities::lang::Lang;
use crate::impls::sqlite_repos::settings::DbSettingRepo;
use crate::impls::sqlite_repos::tests::common::mem_connection;
use crate::proto::db::{IDbRepo, INewDbRepo, ISettingsRepo};

#[test]
fn test_get_nf() {
    let repo = DbSettingRepo::create(mem_connection());
    repo.init().unwrap();
    assert_eq!(
        repo.get_settings(),
        Err(DbError::RequiredValueNotFound(
            "settings.base_type".to_string()
        ))
    );
    assert_eq!(repo.get_size(), Ok(0));
}

#[test]
fn test_get_set() {
    let repo = DbSettingRepo::create(mem_connection());
    repo.init().unwrap();
    let defaults = BaseSettings {
        base_type: BaseType::Words,
        base_version: "1.0".to_string(),
        poet_version: "2".to_string(),
        lang: Lang::Ru,
        description: "hey hey".to_string(),
    };
    repo.init_defaults(defaults.clone()).unwrap();
    assert_eq!(repo.get_settings(), Ok(defaults.clone()));
    let mut updated = defaults.clone();
    updated.poet_version = "abc".to_string();
    repo.set_settings(updated.clone()).unwrap();
    assert_eq!(repo.get_settings(), Ok(updated.clone()));
    repo.init_defaults(defaults).unwrap();
    assert_eq!(repo.get_settings(), Ok(updated));
}

#[test]
fn test_escaped_values() {
    let repo = DbSettingRepo::create(mem_connection());
    repo.init().unwrap();
    let defaults = BaseSettings {
        base_type: BaseType::Words,
        base_version: "\"".to_string(),
        poet_version: "\\".to_string(),
        lang: Lang::Ru,
        description: "'\0\n\tbb".to_string(),
    };
    repo.init_defaults(defaults.clone()).unwrap();
    assert_eq!(repo.get_settings(), Ok(defaults.clone()));
}
