use crate::entities::errors::DbError;
use crate::entities::phrase::NewPhrase;
use crate::impls::sqlite_repos::phrase::DbPhraseRepo;
use crate::impls::sqlite_repos::tests::common::mem_connection;
use crate::proto::db::{IDbRepo, INewDbRepo, IPhraseRepo};

#[test]
fn test_add_get() {
    let repo = DbPhraseRepo::create(mem_connection());
    repo.init().unwrap();
    assert_eq!(repo.get_size(), Ok(0));
    let phrases = &[
        NewPhrase {
            slug: "aabb".to_string(),
            text: "aa bb".to_string(),
            ending: "bb".to_string(),
            last_word: "B".to_string(),
            syl_count: 2,
        },
        NewPhrase {
            slug: "xxyy".to_string(),
            text: "xx yy".to_string(),
            ending: "yy".to_string(),
            last_word: "Y".to_string(),
            syl_count: 1,
        },
    ];
    repo.add_phrases(phrases).unwrap();
    assert_eq!(repo.get_size(), Ok(2));
    assert_eq!(
        NewPhrase::from(repo.get_by_slug(&phrases[0].slug).unwrap()),
        phrases[0],
    );
    assert_eq!(
        NewPhrase::from(repo.get_by_slug(&phrases[1].slug).unwrap()),
        phrases[1],
    );
    assert_eq!(
        NewPhrase::from(repo.get_by_text(&phrases[0].text).unwrap()),
        phrases[0],
    );
    assert_eq!(
        NewPhrase::from(repo.get_by_text(&phrases[1].text).unwrap()),
        phrases[1],
    );
    assert_eq!(repo.get_by_slug(&phrases[0].text), Err(DbError::NotFound));
    assert_eq!(repo.get_by_text(&phrases[0].slug), Err(DbError::NotFound));
}

#[test]
fn test_update_ending() {
    let repo = DbPhraseRepo::create(mem_connection());
    repo.init().unwrap();
    let new_phrase = NewPhrase {
        slug: "aabb".to_string(),
        text: "aa bb".to_string(),
        ending: "bb".to_string(),
        last_word: "B".to_string(),
        syl_count: 2,
    };
    repo.add_phrases(&[new_phrase.clone()]).unwrap();
    let phrase = repo.get_by_slug(&new_phrase.slug).unwrap();
    assert_eq!(phrase.ending, "bb");
    repo.update_ending(phrase.id, "asd").unwrap();
    assert_eq!(repo.get_by_id(phrase.id).unwrap().ending, "asd");
}

#[test]
fn test_update_last_word() {
    let repo = DbPhraseRepo::create(mem_connection());
    repo.init().unwrap();
    let new_phrase = NewPhrase {
        slug: "aabb".to_string(),
        text: "aa bb".to_string(),
        ending: "bb".to_string(),
        last_word: "B".to_string(),
        syl_count: 2,
    };
    repo.add_phrases(&[new_phrase.clone()]).unwrap();
    let phrase = repo.get_by_slug(&new_phrase.slug).unwrap();
    assert_eq!(phrase.last_word, "B");
    repo.update_last_word(phrase.id, "asd").unwrap();
    assert_eq!(repo.get_by_id(phrase.id).unwrap().last_word, "asd");
}

#[test]
fn test_update_syl() {
    let repo = DbPhraseRepo::create(mem_connection());
    repo.init().unwrap();
    let new_phrase = NewPhrase {
        slug: "aabb".to_string(),
        text: "aa bb".to_string(),
        ending: "bb".to_string(),
        last_word: "B".to_string(),
        syl_count: 2,
    };
    repo.add_phrases(&[new_phrase.clone()]).unwrap();
    let phrase = repo.get_by_slug(&new_phrase.slug).unwrap();
    assert_eq!(phrase.syl_count, 2);
    repo.update_syl(phrase.id, 3).unwrap();
    assert_eq!(repo.get_by_id(phrase.id).unwrap().syl_count, 3);
}

#[test]
fn test_get_batch() {
    let repo = DbPhraseRepo::create(mem_connection());
    repo.init().unwrap();
    let phrases = ('a'..='z')
        .map(|key| NewPhrase {
            slug: key.to_string(),
            text: key.to_string(),
            ending: key.to_string(),
            last_word: key.to_string(),
            syl_count: 1,
        })
        .collect::<Vec<_>>();

    repo.add_phrases(&phrases).unwrap();
    let batch = repo.get_id_range(0, 3).unwrap();
    assert_eq!(batch, Some((1, 3)));

    let batch = repo.get_id_range(3, 3).unwrap();
    assert_eq!(batch, Some((4, 6)));

    let batch = repo.get_id_range(25, 3).unwrap();
    assert_eq!(batch, Some((26, 26)));

    let batch = repo.get_id_range(26, 3).unwrap();
    assert!(batch.is_none());
    let batch = repo.get_id_range(27, 3).unwrap();
    assert!(batch.is_none())
}

#[test]
fn test_escape() {
    let repo = DbPhraseRepo::create(mem_connection());
    repo.init().unwrap();
    let new_phrase = NewPhrase {
        slug: "\'a\\bcd\"1".to_string(),
        text: "\'a\\bcd\"2".to_string(),
        ending: "\'a\\bcd\"3".to_string(),
        last_word: "\'a\\bcd\"4".to_string(),
        syl_count: 0,
    };
    repo.add_phrases(&[new_phrase.clone()]).unwrap();
    let phrase = repo.get_by_slug(&new_phrase.slug).unwrap();
    assert_eq!(phrase.slug, new_phrase.slug);
    assert_eq!(phrase.text, new_phrase.text);
    assert_eq!(phrase.ending, new_phrase.ending);
    assert_eq!(phrase.last_word, new_phrase.last_word);
}
