use std::fmt::Write;

use crate::entities::errors::DbResult;
use crate::entities::phrase::{NewPhrase, Phrase};
use crate::impls::sqlite_repos::base::connection::SQLConnection;
use crate::impls::sqlite_repos::base::table_manager::TableManager;
use crate::impls::sqlite_repos::base::table_schema::{SStr, SVec, TableSchema};
use crate::impls::sqlite_repos::base::utils::{escape, join_columns};
use crate::proto::db::{IDbRepo, INewDbRepo, IPhraseRepo};
use crate::sqlite_repos::base::utils::deescape;

#[derive(Clone)]
pub struct DbPhraseRepo {
    manager: TableManager<Table>,
    select_columns: String,
    insert_columns: String,
}

pub(crate) struct Table {}

pub(crate) const C_TEXT: &str = "text";
pub(crate) const C_SLUG: &str = "slug";
pub(crate) const C_ENDING: &str = "ending";
pub(crate) const C_LAST_WORD: &str = "last_word";
pub(crate) const C_SYL: &str = "syl";
const C_ID: &str = "id";
const INSERT_COLUMNS: &[&str] = &[C_TEXT, C_SLUG, C_ENDING, C_LAST_WORD, C_SYL];

impl TableSchema for Table {
    const PK: SStr = C_ID;
    const NAME: SStr = "phrase";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        (C_ID, "INTEGER PRIMARY KEY NOT NULL"),
        (C_TEXT, "TEXT NOT NULL"),
        (C_SLUG, "TEXT NOT NULL"),
        (C_ENDING, "TEXT NOT NULL"),
        (C_LAST_WORD, "TEXT NOT NULL"),
        (C_SYL, "INTEGER NOT NULL"),
    ];
    const INDEXES: SVec<SStr> = &[C_TEXT, C_SLUG, C_ENDING, C_LAST_WORD];
    const UNIQUE: SVec<SStr> = &[C_SLUG, C_TEXT];
}

impl INewDbRepo<SQLConnection> for DbPhraseRepo {
    fn create(connect: SQLConnection) -> Self {
        Self {
            manager: TableManager::create(connect),
            select_columns: join_columns(&[C_ID, C_TEXT, C_SLUG, C_ENDING, C_LAST_WORD, C_SYL]),
            insert_columns: join_columns(INSERT_COLUMNS),
        }
    }
}

impl IDbRepo for DbPhraseRepo {
    fn as_super(&self) -> &dyn IDbRepo {
        &self.manager
    }
}

fn deserialize_phrase(row: &rusqlite::Row) -> rusqlite::Result<Phrase> {
    Ok(Phrase {
        id: row.get(0)?,
        text: deescape(row.get(1)?),
        slug: deescape(row.get(2)?),
        ending: deescape(row.get(3)?),
        last_word: deescape(row.get(4)?),
        syl_count: row.get(5)?,
    })
}

impl IPhraseRepo<SQLConnection> for DbPhraseRepo {
    fn add_phrases(&self, phrases: &[NewPhrase]) -> DbResult<()> {
        if phrases.is_empty() {
            return Ok(());
        }
        let phrase = &phrases[0];
        let mut query = format!(
            r#"INSERT OR IGNORE INTO `{}` ({})
            VALUES ({}, {}, {}, {}, {})"#,
            Table::NAME,
            self.insert_columns,
            escape(&phrase.text),
            escape(&phrase.slug),
            escape(&phrase.ending),
            escape(&phrase.last_word),
            phrase.syl_count,
        );
        for phrase in phrases.iter().skip(1) {
            let _ = write!(
                query,
                ", ({}, {}, {}, {}, {})",
                escape(&phrase.text),
                escape(&phrase.slug),
                escape(&phrase.ending),
                escape(&phrase.last_word),
                phrase.syl_count,
            );
        }
        self.manager.execute(&query)
    }

    fn get_by_id(&self, id: usize) -> DbResult<Phrase> {
        let query = format!(
            "SELECT {} FROM `{}` WHERE `{}` = {}",
            self.select_columns,
            Table::NAME,
            C_ID,
            id,
        );
        self.manager.get_one(&query, deserialize_phrase)
    }

    fn get_by_slug(&self, slug: &str) -> DbResult<Phrase> {
        let query = format!(
            "SELECT {} FROM `{}` WHERE `{}` = {}",
            self.select_columns,
            Table::NAME,
            C_SLUG,
            escape(slug),
        );
        self.manager.get_one(&query, deserialize_phrase)
    }

    fn get_by_text(&self, text: &str) -> DbResult<Phrase> {
        let query = format!(
            "SELECT {} FROM `{}` WHERE `{}` = {}",
            self.select_columns,
            Table::NAME,
            C_TEXT,
            escape(text),
        );
        self.manager.get_one(&query, deserialize_phrase)
    }

    fn update_ending(&self, id: usize, new_end: &str) -> DbResult<()> {
        let query = format!(
            "UPDATE `{}` SET `{}` = {} WHERE `{}` = {}",
            Table::NAME,
            C_ENDING,
            escape(new_end),
            C_ID,
            id,
        );
        self.manager.execute(&query)
    }

    fn update_last_word(&self, id: usize, new_word: &str) -> DbResult<()> {
        let query = format!(
            "UPDATE `{}` SET `{}` = {} WHERE `{}` = {}",
            Table::NAME,
            C_LAST_WORD,
            escape(new_word),
            C_ID,
            id,
        );
        self.manager.execute(&query)
    }

    fn update_syl(&self, id: usize, syl: usize) -> DbResult<()> {
        let query = format!(
            "UPDATE `{}` SET `{}` = {} WHERE `{}` = {}",
            Table::NAME,
            C_SYL,
            syl,
            C_ID,
            id,
        );
        self.manager.execute(&query)
    }

    fn get_id_range(&self, min_id: usize, size: usize) -> DbResult<Option<(usize, usize)>> {
        let query = format!(
            r#"WITH target_seq AS (
                SELECT `{id}` FROM `{tbl}`
                WHERE `{id}` > {min}
                ORDER BY `{id}` LIMIT {size}
            ), first_q AS (
                SELECT * FROM target_seq LIMIT 1
            ), last_q AS (
                SELECT * FROM target_seq ORDER BY `{id}` DESC LIMIT 1
            ) SELECT * FROM first_q UNION ALL select * FROM last_q"#,
            id = Table::PK,
            tbl = Table::NAME,
            min = min_id,
            size = size,
        );
        let rows = self.manager.get_many(&query, |row| row.get(0))?;
        match rows.len() {
            0 => Ok(None),
            2 => Ok(Some((rows[0], rows[1]))),
            _ => unreachable!(),
        }
    }
}
