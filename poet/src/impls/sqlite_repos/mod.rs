pub(crate) mod base;
pub mod phrase;
pub mod rhyme;
pub mod settings;
#[cfg(test)]
pub mod tests;
pub mod word;
