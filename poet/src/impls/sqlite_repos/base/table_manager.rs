use std::marker::PhantomData;

use crate::entities::errors::{DbError, DbResult};
use crate::impls::sqlite_repos::base::connection::SQLConnection;
use crate::impls::sqlite_repos::base::table_schema::TableSchema;
use crate::proto::db::{IDbRepo, INewDbRepo};

pub struct TableManager<Tbl: TableSchema> {
    connection: SQLConnection,
    table: PhantomData<Tbl>,
}

impl<T: TableSchema> Clone for TableManager<T> {
    fn clone(&self) -> Self {
        Self {
            connection: self.connection.clone(),
            table: Default::default(),
        }
    }
}

impl<Tbl: TableSchema> TableManager<Tbl> {
    pub fn get_one<T, F: FnMut(&rusqlite::Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        serializer: F,
    ) -> DbResult<T> {
        let opt_val = self
            .connection
            .get()
            .prepare(query)?
            .query_map((), serializer)?
            .next();
        match opt_val {
            None => Err(DbError::NotFound),
            Some(val) => Ok(val?),
        }
    }

    pub fn get_many<T, F: FnMut(&rusqlite::Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        serializer: F,
    ) -> DbResult<Vec<T>> {
        let result = self
            .connection
            .get()
            .prepare(query)?
            .query_map((), serializer)?
            .collect::<Result<Vec<T>, _>>()?;
        Ok(result)
    }

    pub fn execute(&self, query: &str) -> DbResult<()> {
        self.connection.get().execute(query, ())?;
        Ok(())
    }

    fn create_query(&self) -> String {
        let columns = Tbl::COLUMNS
            .iter()
            .map(|(c_name, c_type)| format!("`{}` {}", c_name, c_type));
        let unique = Tbl::UNIQUE.iter().map(|attr| format!("UNIQUE(`{}`)", attr));
        let f_keys = Tbl::FOREIGN_KEYS.iter().map(|(column, table, ext_column)| {
            format!(
                "FOREIGN KEY (`{}`) REFERENCES `{}`(`{}`)",
                column, table, ext_column,
            )
        });
        let attrs = columns
            .chain(unique)
            .chain(f_keys)
            .collect::<Vec<_>>()
            .join(", ");
        format!("CREATE TABLE IF NOT EXISTS `{}` ({})", Tbl::NAME, attrs,)
    }

    fn drop_table(&self) -> DbResult<()> {
        self.connection
            .get()
            .execute(&format!("DROP TABLE IF EXISTS `{}`", Tbl::NAME), ())?;
        Ok(())
    }

    fn make_index_name(&self, field: &str) -> String {
        format!("{}_{}_index", Tbl::NAME, field)
    }
}

impl<Tbl: TableSchema> INewDbRepo<SQLConnection> for TableManager<Tbl> {
    fn create(connection: SQLConnection) -> Self {
        Self {
            connection,
            table: Default::default(),
        }
    }
}

impl<Tbl: TableSchema> IDbRepo for TableManager<Tbl> {
    fn init(&self) -> DbResult<()> {
        self.connection
            .get()
            .execute(&self.create_query(), ())
            .map_err(|err| DbError::CanNotInitTable(Tbl::NAME, err.to_string()))?;
        Ok(())
    }

    fn recreate_table(&self) -> DbResult<()> {
        self.drop_table()?;
        self.init()
    }

    fn set_indexes(&self) -> DbResult<()> {
        let cnn = self.connection.get();
        for column in Tbl::INDEXES.iter() {
            let index = self.make_index_name(column);
            let query = format!(
                "CREATE INDEX IF NOT EXISTS {} ON `{}` (`{}`)",
                index,
                Tbl::NAME,
                column,
            );
            cnn.execute(&query, ())?;
        }
        Ok(())
    }

    fn drop_indexes(&self) -> DbResult<()> {
        let cnn = self.connection.get();
        for column in Tbl::INDEXES.iter() {
            let index = self.make_index_name(column);
            let query = format!("DROP INDEX IF EXISTS {}", index);
            cnn.execute(&query, ())?;
        }
        Ok(())
    }

    fn get_size(&self) -> DbResult<usize> {
        let cnn = self.connection.get();
        let query = format!("SELECT COUNT(*) FROM `{}`", Tbl::NAME);
        let mut stm = cnn.prepare(&query)?;
        let mut rows = stm.raw_query();
        let row = rows.next()?.unwrap();
        Ok(row.get(0)?)
    }
}
