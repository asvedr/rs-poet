unsafe fn change_borders(text: &mut str) {
    let bts = text.as_bytes_mut();
    bts[0] = b'\'';
    bts[bts.len() - 1] = b'\'';
}

pub fn escape(src: &str) -> String {
    let mut formatted = format!("{:?}", src).replace('\'', "\\d");
    unsafe { change_borders(&mut formatted) }
    formatted
}

// Tab is escaped as \t.
// Carriage return is escaped as \r.
// Line feed is escaped as \n.
// Single quote is escaped as \'.
// Double quote is escaped as \".
// Backslash is escaped as \\.
pub fn deescape(src: String) -> String {
    let mut result = String::new();
    let mut chars = src.chars();
    while let Some(symbol) = chars.next() {
        if symbol != '\\' {
            result.push(symbol);
            continue;
        }
        let next = match chars.next() {
            Some(val) => val,
            _ => break,
        };
        let val = match next {
            't' => '\t',
            'r' => '\r',
            'n' => '\n',
            'd' => '\'',
            '"' => '"',
            '0' => '\0',
            '\\' => '\\',
            _ => panic!("unexpected escaped sequence"),
        };
        result.push(val)
    }
    result
}

#[inline]
pub fn join_columns(columns: &[&str]) -> String {
    columns
        .iter()
        .map(|c| format!("`{}`", c))
        .collect::<Vec<_>>()
        .join(", ")
}
