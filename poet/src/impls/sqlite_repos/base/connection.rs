use std::path::Path;
use std::rc::Rc;

use rusqlite;

use crate::entities::errors::{DbError, DbResult};

#[derive(Clone)]
pub struct SQLConnection {
    inner: Rc<rusqlite::Connection>,
}

impl SQLConnection {
    pub fn new<P: AsRef<Path>>(path: P) -> DbResult<Self> {
        let cnct = rusqlite::Connection::open(path)
            .map_err(|err| DbError::CanNotConnect(err.to_string()))?;
        Ok(Self {
            inner: Rc::new(cnct),
        })
    }

    #[inline(always)]
    pub fn get(&self) -> &rusqlite::Connection {
        &self.inner
    }
}
