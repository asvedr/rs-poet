use std::marker::PhantomData;

use crate::entities::db_settings::{BaseSettings, BaseType};
use crate::entities::errors::{DbError, DbResult};
use crate::entities::lang::Lang;
use crate::entities::phrase::{NewPhrase, Phrase};
use crate::entities::requests::{GetPhraseRequest, GetRhymeRequest};
use crate::proto::db::{IPhraseRepo, IRhymeRepo, ISettingsRepo};
use crate::proto::interface::{IBaseLoader, IPhraseBase};
use crate::utils::common::version;

pub(crate) struct PhraseBase<C, Sr: ISettingsRepo<C>, Pr: IPhraseRepo<C>, Rh: IRhymeRepo<C>> {
    settings_repo: Sr,
    phrase_repo: Pr,
    rhymes_repo: Rh,
    phantom: PhantomData<C>,
}

impl<C, Sr: ISettingsRepo<C>, Pr: IPhraseRepo<C>, Rh: IRhymeRepo<C>> PhraseBase<C, Sr, Pr, Rh> {
    fn init_settings(&self, lang: Lang, description: &str) -> DbResult<()> {
        let defaults = BaseSettings {
            base_type: BaseType::Phrases,
            base_version: "1.0.0".to_string(),
            poet_version: version(),
            lang,
            description: description.to_string(),
        };
        self.settings_repo.init_defaults(defaults)
    }
}

impl<C: Clone, Sr: ISettingsRepo<C>, Pr: IPhraseRepo<C>, Rh: IRhymeRepo<C>> IBaseLoader<C>
    for PhraseBase<C, Sr, Pr, Rh>
{
    fn create(connect: C, lang: Lang, description: &str) -> DbResult<Self> {
        let result = Self {
            settings_repo: Sr::create(connect.clone()),
            phrase_repo: Pr::create(connect.clone()),
            rhymes_repo: Rh::create(connect),
            phantom: Default::default(),
        };
        result.settings_repo.init()?;
        result.phrase_repo.init()?;
        result.rhymes_repo.init()?;
        result.init_settings(lang, description)?;
        result.settings_repo.set_indexes()?;
        Ok(result)
    }

    fn load(connect: C) -> DbResult<Self> {
        let result = Self {
            settings_repo: Sr::create(connect.clone()),
            phrase_repo: Pr::create(connect.clone()),
            rhymes_repo: Rh::create(connect),
            phantom: Default::default(),
        };
        result.settings_repo.init()?;
        result.phrase_repo.init()?;
        result.rhymes_repo.init()?;
        let db_type = result.get_settings()?.base_type;
        if db_type != BaseType::Phrases {
            return Err(DbError::InvalidBaseType(db_type.to_string()));
        }
        Ok(result)
    }
}

impl<C: Clone, Sr: ISettingsRepo<C>, Pr: IPhraseRepo<C>, Rh: IRhymeRepo<C>> IPhraseBase
    for PhraseBase<C, Sr, Pr, Rh>
{
    fn get_settings(&self) -> DbResult<BaseSettings> {
        self.settings_repo.get_settings()
    }

    fn get_phrases_size(&self) -> DbResult<usize> {
        self.phrase_repo.get_size()
    }

    fn get_rhymes_size(&self) -> DbResult<usize> {
        self.rhymes_repo.get_size()
    }

    fn add_phrases(&mut self, phrases: &[NewPhrase]) -> DbResult<()> {
        self.phrase_repo.add_phrases(phrases)
    }

    fn get_by_slug(&self, slug: &str) -> DbResult<Phrase> {
        self.phrase_repo.get_by_slug(slug)
    }

    fn get_by_text(&self, text: &str) -> DbResult<Phrase> {
        self.phrase_repo.get_by_text(text)
    }

    fn update_ending(&mut self, id: usize, new_end: &str) -> DbResult<()> {
        self.phrase_repo.update_ending(id, new_end)
    }

    fn update_last_word(&mut self, id: usize, new_word: &str) -> DbResult<()> {
        self.phrase_repo.update_last_word(id, new_word)
    }

    fn update_syl(&mut self, id: usize, syl: usize) -> DbResult<()> {
        self.phrase_repo.update_syl(id, syl)
    }

    fn recalculate_rhymes(&mut self, batch_size: usize, max_syl_diff: usize) -> DbResult<()> {
        self.rhymes_repo
            .recalculate_rhymes(batch_size, max_syl_diff)
    }

    fn get_rhyme_to_phrase(
        &self,
        phrase: &Phrase,
        request: GetRhymeRequest,
    ) -> DbResult<Option<Phrase>> {
        self.rhymes_repo.get_rhyme_to_phrase(phrase, request)
    }

    fn get_random_phrase(&self, request: GetPhraseRequest) -> DbResult<Option<Phrase>> {
        self.rhymes_repo.get_random_phrase(request)
    }
}
