use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::rc::Rc;

use crate::entities::errors::DLResult;
use crate::entities::fun_iterator::{FunIterator, NextIter};
use crate::entities::lang::Lang;
use crate::entities::word::Word;
use crate::impls::lang_utils::ru::RuUtils;
use crate::proto::dict::ISpecificDictLoader;

const STRONG_SYMBOL: char = '\'';

struct Inner {
    word_processor: RuUtils,
    available_symbols: HashSet<char>,
}

#[derive(Clone)]
pub struct Loader {
    inner: Rc<Inner>,
}

impl Loader {
    pub fn new() -> Self {
        let processor = RuUtils::new(STRONG_SYMBOL);
        let mut available_symbols = processor.alphabet().clone();
        for symbol in processor.alphabet() {
            let upper = symbol.to_uppercase().next().unwrap();
            available_symbols.insert(upper);
        }
        available_symbols.insert(STRONG_SYMBOL);
        let inner = Inner {
            word_processor: processor,
            available_symbols,
        };
        Self {
            inner: Rc::new(inner),
        }
    }

    fn parse_line(&self, original_line: &str) -> Option<Word> {
        let line = original_line.trim();
        if line.is_empty() {
            return None;
        }
        let word_src = line.split('|').nth(1).unwrap().trim();
        for sym in word_src.chars() {
            if !self.inner.available_symbols.contains(&sym) {
                eprintln!("WARNING: Invalid line: {:?}", original_line);
                return None;
            }
        }
        let (syl_count, end) = match self.inner.word_processor.get_syl_count_and_ending(word_src) {
            Some(val) => val,
            None => {
                eprintln!("WARNING: Invalid line: {:?}", original_line);
                return None;
            }
        };
        Some(Word {
            text: word_src.replace(STRONG_SYMBOL, ""),
            syl_count,
            ending: end,
        })
    }

    fn parse_buffer(&self, mut handler: BufReader<File>) -> FunIterator<DLResult<Word>> {
        let loader = self.clone();
        FunIterator::new(move || {
            let mut line = String::new();
            if let Err(err) = handler.read_line(&mut line) {
                return NextIter::Single(Err(err.into()));
            }
            if line.is_empty() {
                return NextIter::Stop;
            }
            let tail = loader.parse_buffer(handler);
            match loader.parse_line(&line) {
                None => NextIter::Next(tail),
                Some(word) => NextIter::Value(Ok(word), tail),
            }
        })
    }
}

impl ISpecificDictLoader<Word> for Loader {
    fn lang(&self) -> Lang {
        Lang::Ru
    }

    fn ext(&self) -> &'static str {
        "hagen"
    }

    fn parse_file(&self, file: File) -> FunIterator<DLResult<Word>> {
        self.parse_buffer(BufReader::new(file))
    }
}
