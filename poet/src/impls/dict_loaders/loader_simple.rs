use std::fs::File;
use std::io::{BufRead, BufReader};
use std::rc::Rc;

use crate::entities::errors::{DLResult, DictLoaderError};
use crate::entities::fun_iterator::{FunIterator, NextIter};
use crate::entities::lang::Lang;
use crate::entities::word::Word;
use crate::impls::lang_utils::ru::RuUtils;
use crate::proto::dict::ISpecificDictLoader;

const STRONG_SYMBOL: char = '`';

#[derive(Clone)]
pub struct Loader {
    word_processor: Rc<RuUtils>,
}

impl Loader {
    pub fn new() -> Self {
        Self {
            word_processor: Rc::new(RuUtils::new(STRONG_SYMBOL)),
        }
    }

    fn parse_line(&self, original: String) -> FunIterator<DLResult<Word>> {
        let line = original.trim();
        if line.is_empty() {
            return FunIterator::empty();
        }
        let mut result = Vec::new();
        for word_src in line.split(',') {
            let word = self.parse_word(word_src);
            let is_err = word.is_err();
            result.push(word);
            if is_err {
                break;
            }
        }
        FunIterator::from_vec(result)
    }

    fn parse_word(&self, word_src: &str) -> DLResult<Word> {
        let opt_pair = self.word_processor.get_syl_count_and_ending(word_src);
        let (syl_count, end) = match opt_pair {
            None => {
                let err =
                    DictLoaderError::InvalidData(format!("Invalid word in dict: {:?}", word_src));
                return Err(err);
            }
            Some(val) => val,
        };
        let word = Word {
            text: word_src.replace(STRONG_SYMBOL, ""),
            syl_count,
            ending: end,
        };
        Ok(word)
    }

    fn parse_buffer(&self, mut buffer: BufReader<File>) -> FunIterator<DLResult<Word>> {
        let loader = self.clone();
        FunIterator::new(move || {
            let mut line = String::new();
            if let Err(err) = buffer.read_line(&mut line) {
                return NextIter::Single(Err(err.into()));
            }
            if line.is_empty() {
                return NextIter::Stop;
            }
            let fun = loader.parse_line(line).join(loader.parse_buffer(buffer));
            NextIter::Next(fun)
        })
    }
}

impl ISpecificDictLoader<Word> for Loader {
    fn lang(&self) -> Lang {
        Lang::Ru
    }

    fn ext(&self) -> &'static str {
        "simple"
    }

    fn parse_file(&self, file: File) -> FunIterator<DLResult<Word>> {
        self.parse_buffer(BufReader::new(file))
    }
}
