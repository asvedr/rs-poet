use crate::entities::lang::Lang;
use crate::entities::phrase::NewPhrase;
use crate::entities::word::Word;
use crate::proto::dict::ISpecificDictLoader;

mod loader_csv;
mod loader_forms;
mod loader_hagen;
#[cfg(feature = "all_dicts")]
mod loader_js_en;
#[cfg(feature = "all_dicts")]
mod loader_js_phrase;
mod loader_simple;
#[cfg(test)]
mod tests;

pub fn word_loaders() -> Vec<Box<dyn ISpecificDictLoader<Word>>> {
    vec![
        Box::new(loader_forms::Loader::new()),
        Box::new(loader_hagen::Loader::new()),
        Box::new(loader_simple::Loader::new()),
        #[cfg(feature = "all_dicts")]
        Box::new(loader_js_en::Loader::new()),
        Box::new(loader_csv::Loader::new(Lang::Ru)),
        Box::new(loader_csv::Loader::new(Lang::En)),
    ]
}

pub fn phrase_loaders() -> Vec<Box<dyn ISpecificDictLoader<NewPhrase>>> {
    vec![
        #[cfg(feature = "all_dicts")]
        Box::new(loader_js_phrase::Loader::new(Lang::Ru)),
        #[cfg(feature = "all_dicts")]
        Box::new(loader_js_phrase::Loader::new(Lang::En)),
    ]
}
