use std::fs::File;
use std::io::Read;

use json::object::Object;
use json::JsonValue;

use crate::entities::errors::{DLResult, DictLoaderError};
use crate::entities::lang::Lang;
use crate::entities::phrase::NewPhrase;
use crate::proto::dict::ISpecificDictLoader;
use crate::FunIterator;

pub struct Loader {
    lang: Lang,
}

impl Loader {
    pub fn new(lang: Lang) -> Self {
        Self { lang }
    }

    fn fetch_json(mut file: File) -> DLResult<json::JsonValue> {
        let mut buffer = String::new();
        file.read_to_string(&mut buffer)?;
        let val = json::parse(&buffer)
            .map_err(|_| DictLoaderError::InvalidData("not a json".to_string()))?;
        Ok(val)
    }

    fn get_field<T, F: Fn(&JsonValue) -> Option<T>>(
        map: &Object,
        key: &str,
        convert: F,
    ) -> DLResult<T> {
        let item = match map.get(key) {
            None => {
                let err = format!("item {:?} has no field {}", map, key,);
                return Err(DictLoaderError::InvalidData(err));
            }
            Some(val) => val,
        };
        match convert(item) {
            None => {
                let err = format!("item {:?}, field {} invalid type", map, key,);
                return Err(DictLoaderError::InvalidData(err));
            }
            Some(val) => Ok(val),
        }
    }

    fn parse_item(item: JsonValue) -> DLResult<NewPhrase> {
        let map = match item {
            JsonValue::Object(val) => val,
            other => {
                let err = format!("invalid json item(not a dict): {:?}", other);
                return Err(DictLoaderError::InvalidData(err));
            }
        };
        fn js_to_string(val: &JsonValue) -> Option<String> {
            Some(val.as_str()?.to_string())
        }
        let phrase = NewPhrase {
            slug: Self::get_field(&map, "sl", js_to_string)?,
            text: Self::get_field(&map, "te", js_to_string)?,
            ending: Self::get_field(&map, "en", js_to_string)?,
            last_word: Self::get_field(&map, "la", js_to_string)?,
            syl_count: Self::get_field(&map, "sy", JsonValue::as_usize)?,
        };
        Ok(phrase)
    }
}

impl ISpecificDictLoader<NewPhrase> for Loader {
    fn lang(&self) -> Lang {
        self.lang
    }

    fn ext(&self) -> &'static str {
        "js-ph"
    }

    fn parse_file(&self, file: File) -> FunIterator<DLResult<NewPhrase>> {
        let js = match Self::fetch_json(file) {
            Ok(val) => val,
            Err(err) => return FunIterator::from_vec(vec![Err(err)]),
        };
        let list = match js {
            JsonValue::Array(val) => val,
            _ => {
                let err = "invalid json toplevel(not a list)";
                let err = DictLoaderError::InvalidData(err.to_string());
                return FunIterator::from_vec(vec![Err(err)]);
            }
        };
        let mut result = Vec::new();
        for item in list {
            result.push(Self::parse_item(item));
        }
        FunIterator::from_vec(result)
    }
}
