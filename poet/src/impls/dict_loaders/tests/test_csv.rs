use crate::entities::lang::Lang;
use crate::entities::word::Word;
use crate::impls::dict_loaders::loader_csv::Loader;
use crate::proto::dict::ISpecificDictLoader;
use std::fs::File;

#[test]
fn test_load() {
    let loader = Loader::new(Lang::En);
    let file = File::open("test_data/test.csv.txt").unwrap();
    let gen = loader.parse_file(file);
    let collected = gen.collect::<Result<Vec<_>, _>>().unwrap();
    let expected = &[
        Word {
            text: "'bout".to_string(),
            syl_count: 2,
            ending: "UT".to_string(),
        },
        Word {
            text: "aaberg".to_string(),
            syl_count: 5,
            ending: "AA1 B ER0 G".to_string(),
        },
    ];
    assert_eq!(collected, expected);
}
