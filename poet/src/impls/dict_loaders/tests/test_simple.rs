use crate::entities::word::Word;
use crate::impls::dict_loaders::loader_simple::Loader;
use crate::proto::dict::ISpecificDictLoader;
use std::fs::File;

#[test]
fn test_load() {
    let loader = Loader::new();
    let file = File::open("test_data/test.simple.txt").unwrap();
    let collected = loader
        .parse_file(file)
        .collect::<Result<Vec<_>, _>>()
        .unwrap();
    let expected = &[
        Word {
            text: "помню".to_string(),
            syl_count: 2,
            ending: "омну".to_string(),
        },
        Word {
            text: "чудное".to_string(),
            syl_count: 3,
            ending: "удноэ".to_string(),
        },
        Word {
            text: "мгновенье".to_string(),
            syl_count: 3,
            ending: "эньэ".to_string(),
        },
        Word {
            text: "перед".to_string(),
            syl_count: 2,
            ending: "эрэд".to_string(),
        },
    ];
    assert_eq!(collected, expected);
}
