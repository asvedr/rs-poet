use std::fs::File;

use crate::entities::word::Word;
use crate::impls::dict_loaders::loader_js_en::Loader;
use crate::proto::dict::ISpecificDictLoader;

#[test]
fn test_load() {
    let loader = Loader::new();
    let file = File::open("test_data/test.js-eng-code.txt").unwrap();
    let collected = loader
        .parse_file(file)
        .collect::<Result<Vec<_>, _>>()
        .unwrap();
    let expected = &[
        Word {
            text: "'cause".to_string(),
            syl_count: 1,
            ending: "AH0 Z".to_string(),
        },
        Word {
            text: "a".to_string(),
            syl_count: 1,
            ending: "AH0".to_string(),
        },
        Word {
            text: "a's".to_string(),
            syl_count: 1,
            ending: "EY1 Z".to_string(),
        },
        Word {
            text: "aaberg".to_string(),
            syl_count: 2,
            ending: "AA1 B ER0 G".to_string(),
        },
        Word {
            text: "aachen".to_string(),
            syl_count: 2,
            ending: "AA1 K AH0 N".to_string(),
        },
        Word {
            text: "aaker".to_string(),
            syl_count: 2,
            ending: "AA1 K ER0".to_string(),
        },
    ];
    for (c, e) in collected.iter().zip(expected.iter()) {
        assert_eq!(c, e);
    }
    assert_eq!(collected, expected);
}
