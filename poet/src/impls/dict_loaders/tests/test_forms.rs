use std::fs::File;

use crate::entities::word::Word;
use crate::impls::dict_loaders::loader_forms::Loader;
use crate::proto::dict::ISpecificDictLoader;

#[test]
fn test_load() {
    let loader = Loader::new();
    let file = File::open("test_data/test.forms.txt").unwrap();
    let gen = loader.parse_file(file);
    let expected = &[
        Word {
            text: "абажурный".to_string(),
            syl_count: 4,
            ending: "урний".to_string(),
        },
        Word {
            text: "абажурная".to_string(),
            syl_count: 5,
            ending: "урнаа".to_string(),
        },
        Word {
            text: "абажурное".to_string(),
            syl_count: 5,
            ending: "урноэ".to_string(),
        },
        Word {
            text: "абаз".to_string(),
            syl_count: 2,
            ending: "аз".to_string(),
        },
        Word {
            text: "абазы".to_string(),
            syl_count: 3,
            ending: "ази".to_string(),
        },
        Word {
            text: "абаза".to_string(),
            syl_count: 3,
            ending: "аза".to_string(),
        },
        Word {
            text: "абазов".to_string(),
            syl_count: 3,
            ending: "азов".to_string(),
        },
        Word {
            text: "абазу".to_string(),
            syl_count: 3,
            ending: "азу".to_string(),
        },
    ];
    let fetched = gen.into_iter().collect::<Result<Vec<Word>, _>>().unwrap();
    assert_eq!(fetched, expected)
}
