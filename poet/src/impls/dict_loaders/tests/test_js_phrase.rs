use std::fs::File;

use crate::entities::lang::Lang;
use crate::entities::phrase::NewPhrase;
use crate::impls::dict_loaders::loader_js_phrase::Loader;
use crate::proto::dict::ISpecificDictLoader;

#[test]
fn test_load() {
    let loader = Loader::new(Lang::En);
    let file = File::open("test_data/test.js-ph.txt").unwrap();
    let collected = loader
        .parse_file(file)
        .collect::<Result<Vec<_>, _>>()
        .unwrap();
    let expected = &[
        NewPhrase {
            slug: "helloworld".to_string(),
            text: "Hello World".to_string(),
            ending: "ld".to_string(),
            last_word: "world".to_string(),
            syl_count: 3,
        },
        NewPhrase {
            slug: "beebaa".to_string(),
            text: "beebaa".to_string(),
            ending: "aa".to_string(),
            last_word: "beebaa".to_string(),
            syl_count: 2,
        },
    ];
    assert_eq!(collected, expected);
}
