use crate::entities::word::Word;
use crate::impls::dict_loaders::loader_hagen::Loader;
use crate::proto::dict::ISpecificDictLoader;
use std::fs::File;

#[test]
fn test_load() {
    let loader = Loader::new();
    let file = File::open("test_data/test.hagen.txt").unwrap();
    let gen = loader.parse_file(file);
    let collected = gen.collect::<Result<Vec<_>, _>>().unwrap();
    let expected = &[
        Word {
            text: "успение".to_string(),
            syl_count: 4,
            ending: "эниэ".to_string(),
        },
        Word {
            text: "успения".to_string(),
            syl_count: 4,
            ending: "эниа".to_string(),
        },
        Word {
            text: "успению".to_string(),
            syl_count: 4,
            ending: "эниу".to_string(),
        },
        Word {
            text: "успение".to_string(),
            syl_count: 4,
            ending: "эниэ".to_string(),
        },
        Word {
            text: "успех".to_string(),
            syl_count: 2,
            ending: "эх".to_string(),
        },
        Word {
            text: "успеха".to_string(),
            syl_count: 3,
            ending: "эха".to_string(),
        },
        Word {
            text: "успеху".to_string(),
            syl_count: 3,
            ending: "эху".to_string(),
        },
    ];
    assert_eq!(collected, expected);
}
