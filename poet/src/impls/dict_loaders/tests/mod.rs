mod test_csv;
mod test_forms;
mod test_hagen;
#[cfg(feature = "all_dicts")]
mod test_js_en;
#[cfg(feature = "all_dicts")]
mod test_js_phrase;
mod test_simple;
