use std::collections::HashSet;
use std::fs::File;
use std::io::Read;

use json;
use json::JsonValue;

use crate::entities::errors::{DLResult, DictLoaderError};
use crate::entities::fun_iterator::FunIterator;
use crate::entities::lang::Lang;
use crate::entities::word::Word;
use crate::proto::dict::ISpecificDictLoader;

const VOWELS: &str = "EYUIOA";
const INVALID_SUFFIX: &str = "abbrev";

pub struct Loader {
    vowels: HashSet<char>,        // set('EYUIOA')
    valid_symbols: HashSet<char>, // = {chr(x) for x in range(ord('a'), ord('z') + 1)} | {"'"}
}

impl Loader {
    pub fn new() -> Self {
        let mut valid_symbols: HashSet<char> = ('a'..='z').collect();
        valid_symbols.insert('\'');
        Self {
            vowels: VOWELS.chars().collect(),
            valid_symbols,
        }
    }

    fn make_ending(&self, sounds: &[&str]) -> Option<String> {
        // второй гласный звук с конца?
        let mut vowels = Vec::new();
        for i in (0..sounds.len()).rev()
        /*reversed(range(len(sounds)))*/
        {
            let first_char = sounds[i].chars().next().unwrap();
            if self.vowels.contains(&first_char) {
                vowels.push(i);
                if vowels.len() == 2 {
                    break;
                }
            }
        }
        if let Some(index) = vowels.last() {
            Some(sounds[*index..].join(" "))
        } else {
            None
        }
    }

    fn make_word(&self, text: String, transcription: String) -> Option<Word> {
        let transcription = transcription.split('#').next().unwrap().trim();
        let sounds = transcription.split(' ').collect::<Vec<_>>();
        let syl_count = sounds
            .iter()
            .filter(|s| self.vowels.contains(&s.chars().next().unwrap()))
            .count();
        let end = self.make_ending(&sounds)?;
        Some(Word {
            text,
            syl_count,
            ending: end,
        })
    }

    fn valid_word(&self, word: &str) -> bool {
        for sym in word.chars() {
            if !self.valid_symbols.contains(&sym) {
                return false;
            }
        }
        true
    }

    fn valid_transcription(&self, transcription: &str) -> bool {
        let split = transcription.split('#').collect::<Vec<_>>();
        split.len() == 1 || split[1] != INVALID_SUFFIX
    }

    fn fetch_json(mut file: File) -> DLResult<json::JsonValue> {
        let mut buffer = String::new();
        file.read_to_string(&mut buffer)?;
        let val = json::parse(&buffer)
            .map_err(|_| DictLoaderError::InvalidData("not a json".to_string()))?;
        Ok(val)
    }
}

impl ISpecificDictLoader<Word> for Loader {
    fn lang(&self) -> Lang {
        Lang::En
    }

    fn ext(&self) -> &'static str {
        "js-eng-code"
    }

    fn parse_file(&self, file: File) -> FunIterator<DLResult<Word>> {
        let js = match Self::fetch_json(file) {
            Ok(val) => val,
            Err(err) => return FunIterator::from_vec(vec![Err(err)]),
        };
        let map = match js {
            JsonValue::Object(val) => val,
            _ => {
                let err = "invalid json toplevel(not a dict)";
                let err = DictLoaderError::InvalidData(err.to_string());
                return FunIterator::from_vec(vec![Err(err)]);
            }
        };
        let mut result = Vec::new();
        for (word, transcription) in map.iter() {
            let mut processed = None;
            let transcription = match transcription.as_str() {
                Some(val) => val,
                _ => {
                    let err = format!("transcription is not string: {:?}", transcription);
                    let err = DictLoaderError::InvalidData(err);
                    return FunIterator::from_vec(vec![Err(err)]);
                }
            };
            if self.valid_word(word) && self.valid_transcription(transcription) {
                processed = self.make_word(word.to_string(), transcription.to_string());
            }
            if let Some(word) = processed {
                result.push(Ok(word))
            }
        }
        FunIterator::from_vec(result)
    }
}
