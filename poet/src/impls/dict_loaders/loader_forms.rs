use std::fs::File;
use std::io::{BufRead, BufReader};
use std::rc::Rc;

use crate::entities::errors::DLResult;
use crate::entities::fun_iterator::{FunIterator, NextIter};
use crate::entities::lang::Lang;
use crate::entities::word::Word;
use crate::impls::lang_utils::ru::RuUtils;
use crate::proto::dict::ISpecificDictLoader;

const STRONG_SYMBOL: char = '\'';
const UNUSED_SYMBOL: &str = "`";

#[derive(Clone)]
pub struct Loader {
    word_processor: Rc<RuUtils>,
}

impl Loader {
    pub fn new() -> Self {
        Self {
            word_processor: Rc::new(RuUtils::new(STRONG_SYMBOL)),
        }
    }

    fn parse_buffer(&self, mut handler: BufReader<File>) -> FunIterator<DLResult<Word>> {
        let loader = self.clone();
        FunIterator::new(move || {
            let mut line = String::new();
            if let Err(err) = handler.read_line(&mut line) {
                return NextIter::Single(Err(err.into()));
            };
            if line.is_empty() {
                return NextIter::Stop;
            }
            let gen_head = loader.parse_line(line);
            let gen_tail = loader.parse_buffer(handler);
            NextIter::Next(gen_head.join(gen_tail))
        })
    }

    fn parse_line(&self, line: String) -> FunIterator<DLResult<Word>> {
        let after_first_split = line.split('#').nth(1);
        let split = match after_first_split {
            Some(val) => val.split(','),
            _ => {
                eprintln!("WARNING FOUND invalid line: {:?}", line);
                return FunIterator::empty();
            }
        };
        let mut result = Vec::new();
        for word_src in split {
            let replaced = word_src.replace(UNUSED_SYMBOL, "");
            let clean_text = replaced.trim();
            let pair_opt = self.word_processor.get_syl_count_and_ending(clean_text);
            let (syl_count, end) = match pair_opt {
                Some(val) => val,
                _ => continue,
            };
            let text = clean_text.replace(STRONG_SYMBOL, "");
            let word = Word {
                text,
                syl_count,
                ending: end,
            };
            result.push(Ok(word))
        }
        FunIterator::from_vec(result)
    }
}

impl ISpecificDictLoader<Word> for Loader {
    fn lang(&self) -> Lang {
        Lang::Ru
    }

    fn ext(&self) -> &'static str {
        "forms"
    }

    fn parse_file(&self, file: File) -> FunIterator<DLResult<Word>> {
        let buffer = BufReader::new(file);
        self.parse_buffer(buffer)
    }
}
