use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str::FromStr;

use crate::entities::errors::{DLResult, DictLoaderError};
use crate::entities::fun_iterator::NextIter;
use crate::entities::lang::Lang;
use crate::entities::word::Word;
use crate::proto::dict::ISpecificDictLoader;
use crate::FunIterator;

#[derive(Clone)]
pub struct Loader {
    lang: Lang,
}

impl Loader {
    pub fn new(lang: Lang) -> Self {
        Self { lang }
    }

    fn parse_buffer(mut source: BufReader<File>) -> FunIterator<DLResult<Word>> {
        FunIterator::new(move || {
            let mut line = String::new();
            if let Err(err) = source.read_line(&mut line) {
                return NextIter::Single(Err(err.into()));
            }
            if line.is_empty() {
                return NextIter::Stop;
            }
            let stripped = line.trim();
            if stripped.is_empty() {
                return NextIter::Next(Self::parse_buffer(source));
            }
            let word = Self::parse_line(stripped);
            if word.is_err() {
                NextIter::Single(word)
            } else {
                NextIter::Value(word, Self::parse_buffer(source))
            }
        })
    }

    fn parse_line(line: &str) -> DLResult<Word> {
        let line = line.split(',').collect::<Vec<_>>();
        if line.len() != 3 {
            let msg = format!("invalid line: {:?}", line);
            return Err(DictLoaderError::InvalidData(msg));
        }
        let text = line[0].to_string();
        let syl = match usize::from_str(line[1]) {
            Ok(val) => val,
            Err(_) => {
                let msg = format!("invalid line: {:?}", line);
                return Err(DictLoaderError::InvalidData(msg));
            }
        };
        let ending = line[2].to_string();
        Ok(Word {
            text,
            syl_count: syl,
            ending,
        })
    }
}

impl ISpecificDictLoader<Word> for Loader {
    fn lang(&self) -> Lang {
        self.lang
    }

    fn ext(&self) -> &'static str {
        "csv"
    }

    fn parse_file(&self, file: File) -> FunIterator<DLResult<Word>> {
        Self::parse_buffer(BufReader::new(file))
    }
}
