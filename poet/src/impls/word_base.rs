use std::marker::PhantomData;

use crate::entities::db_settings::{BaseSettings, BaseType};
use crate::entities::errors::{DbError, DbResult};
use crate::entities::lang::Lang;
use crate::entities::word::Word;
use crate::proto::db::{ISettingsRepo, IWordRepo};
use crate::proto::interface::{IBaseLoader, IWordBase};
use crate::utils::common::version;

pub(crate) struct WordBase<C, Sr: ISettingsRepo<C>, Wr: IWordRepo<C>> {
    settings_repo: Sr,
    word_repo: Wr,
    phantom: PhantomData<C>,
}

impl<C, Sr: ISettingsRepo<C>, Wr: IWordRepo<C>> WordBase<C, Sr, Wr> {
    fn init_settings(&self, lang: Lang, description: &str) -> DbResult<()> {
        self.settings_repo.init_defaults(BaseSettings {
            base_type: BaseType::Words,
            base_version: "1.0.0".to_string(),
            poet_version: version(),
            lang,
            description: description.to_string(),
        })
    }
}

impl<C: Clone, Sr: ISettingsRepo<C>, Wr: IWordRepo<C>> IBaseLoader<C> for WordBase<C, Sr, Wr> {
    fn create(connect: C, lang: Lang, description: &str) -> DbResult<Self> {
        let result = Self {
            word_repo: Wr::create(connect.clone()),
            settings_repo: Sr::create(connect),
            phantom: Default::default(),
        };
        result.settings_repo.init()?;
        result.word_repo.init()?;
        result.init_settings(lang, description)?;
        result.settings_repo.set_indexes()?;
        result.word_repo.set_indexes()?;
        Ok(result)
    }

    fn load(connect: C) -> DbResult<Self> {
        let result = Self {
            word_repo: Wr::create(connect.clone()),
            settings_repo: Sr::create(connect),
            phantom: Default::default(),
        };
        result.settings_repo.init()?;
        result.word_repo.init()?;
        let db_type = result.get_settings()?.base_type;
        if db_type != BaseType::Words {
            return Err(DbError::InvalidBaseType(db_type.to_string()));
        }
        Ok(result)
    }
}

impl<C, Sr: ISettingsRepo<C>, Wr: IWordRepo<C>> IWordBase for WordBase<C, Sr, Wr> {
    fn get_settings(&self) -> DbResult<BaseSettings> {
        self.settings_repo.get_settings()
    }

    fn get_size(&self) -> DbResult<usize> {
        self.word_repo.get_size()
    }

    fn add_words(&mut self, words: &[Word]) -> DbResult<()> {
        self.word_repo.add_words(words)
    }

    fn get_word(&self, text: &str) -> DbResult<Word> {
        self.word_repo.get_word(text)
    }

    fn get_words(&self, texts: &[String]) -> DbResult<Vec<Word>> {
        self.word_repo.get_words(texts)
    }

    fn update_syl(&mut self, text: &str, new_syl: usize) -> DbResult<()> {
        self.word_repo.update_syl(text, new_syl)
    }

    fn update_ending(&mut self, text: &str, new_end: &str) -> DbResult<()> {
        self.word_repo.update_ending(text, new_end)
    }
}
