use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::rc::Rc;

use crate::entities::errors::{PPResult, PhraseProcessorError};
use crate::entities::fun_iterator::NextIter;
use crate::entities::phrase::NewPhrase;
use crate::impls::phrase_makers::{get_maker, get_maker_params};
use crate::proto::phrase_processor::{IPhraseMaker, IPhraseProcessor};
use crate::{FunIterator, IWordBase};

pub struct PhraseProcessor {
    word_base: Rc<Box<dyn IWordBase>>,
}

struct Context {
    word_base: Rc<Box<dyn IWordBase>>,
    maker: Box<dyn IPhraseMaker>,
}

impl Context {
    #[inline(always)]
    fn process_line(&self, line: &str) -> PPResult<NewPhrase> {
        self.maker.make_phrase(&**self.word_base, line)
    }
}

impl PhraseProcessor {
    pub fn new(word_base: Box<dyn IWordBase>) -> Self {
        Self {
            word_base: Rc::new(word_base),
        }
    }

    fn process_buffer(ctx: Context, mut file: BufReader<File>) -> FunIterator<PPResult<NewPhrase>> {
        FunIterator::new(|| {
            let mut buffer = String::new();
            if let Err(err) = file.read_line(&mut buffer) {
                return NextIter::Single(Err(err.into()));
            };
            if buffer.is_empty() {
                return NextIter::Stop;
            }
            let stripped = buffer.trim();
            if stripped.is_empty() {
                return NextIter::Next(Self::process_buffer(ctx, file));
            }
            let val = match ctx.process_line(stripped) {
                Err(PhraseProcessorError::Filtered) => {
                    return NextIter::Next(Self::process_buffer(ctx, file))
                }
                Err(PhraseProcessorError::DbError(err)) => {
                    return NextIter::Single(Err(PhraseProcessorError::DbError(err)))
                }
                other => other,
            };
            let next = Self::process_buffer(ctx, file);
            NextIter::Value(val, next)
        })
    }
}

impl IPhraseProcessor for PhraseProcessor {
    fn load_file(
        &self,
        path: &Path,
        params: Vec<(String, String)>,
    ) -> PPResult<FunIterator<PPResult<NewPhrase>>> {
        let settings = self.word_base.get_settings()?;
        let maker = get_maker(settings.lang, params)?;
        let ctx = Context {
            word_base: self.word_base.clone(),
            maker,
        };
        let file = BufReader::new(File::open(path)?);
        Ok(Self::process_buffer(ctx, file))
    }
    fn get_params(&self) -> PPResult<Vec<(&'static str, &'static str)>> {
        let settings = self.word_base.get_settings()?;
        Ok(get_maker_params(settings.lang))
    }
}
