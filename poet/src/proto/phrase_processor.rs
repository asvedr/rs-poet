use crate::entities::errors::PPResult;
use crate::entities::phrase::NewPhrase;
use crate::{FunIterator, IWordBase};
use std::path::Path;

// fn prewords_euristics(&self) -> Vec<PrewordHeuristic> {
//     default_prewords_euristics()
// }
// fn before_ending_euristics(&self) -> Vec<WordHeuristic> {
//     default_before_ending_euristics()
// }
// fn after_ending_euristics(&self) -> Vec<WordHeuristic> {
//     default_after_ending_euristics()
// }

pub trait IPhraseMakerFactory {
    fn create(params: Vec<(String, String)>) -> PPResult<Box<dyn IPhraseMaker>>;
    fn params() -> Vec<(&'static str, &'static str)>;
}

pub trait IPhraseMaker {
    fn make_phrase(&self, word_base: &dyn IWordBase, source: &str) -> PPResult<NewPhrase>;
}

pub trait IPhraseProcessor {
    fn load_file(
        &self,
        path: &Path,
        params: Vec<(String, String)>,
    ) -> PPResult<FunIterator<PPResult<NewPhrase>>>;
    fn get_params(&self) -> PPResult<Vec<(&'static str, &'static str)>>;
}
