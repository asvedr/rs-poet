use crate::entities::errors::PoetError;
use crate::entities::verse_schema::VerseSchema;

pub trait IGenerator {
    fn generate_verse(
        &mut self,
        schema: &VerseSchema,
        max_tries: usize,
    ) -> Result<String, PoetError>;
}

pub trait IAnalyzer {
    fn recognize_schema(&self, verse: &str) -> Result<Option<VerseSchema>, PoetError>;
}
