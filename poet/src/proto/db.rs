use crate::entities::db_settings::BaseSettings;
use crate::entities::errors::DbResult;
use crate::entities::phrase::{NewPhrase, Phrase};
use crate::entities::requests::{GetPhraseRequest, GetRhymeRequest};
use crate::entities::word::Word;

pub(crate) trait INewDbRepo<Connect> {
    fn create(connect: Connect) -> Self;
}

pub(crate) trait IDbRepo {
    fn as_super(&self) -> &dyn IDbRepo {
        unimplemented!()
    }
    fn init(&self) -> DbResult<()> {
        self.as_super().init()
    }
    fn recreate_table(&self) -> DbResult<()> {
        self.as_super().recreate_table()
    }
    fn set_indexes(&self) -> DbResult<()> {
        self.as_super().set_indexes()
    }
    fn drop_indexes(&self) -> DbResult<()> {
        self.as_super().drop_indexes()
    }
    fn get_size(&self) -> DbResult<usize> {
        self.as_super().get_size()
    }
}

pub(crate) trait ISettingsRepo<C>: IDbRepo + INewDbRepo<C> {
    fn init_defaults(&self, settings: BaseSettings) -> DbResult<()>;
    fn set_settings(&self, settings: BaseSettings) -> DbResult<()>;
    fn get_settings(&self) -> DbResult<BaseSettings>;
}

pub(crate) trait IWordRepo<C>: IDbRepo + INewDbRepo<C> {
    fn add_words(&self, words: &[Word]) -> DbResult<()>;
    fn get_word(&self, text: &str) -> DbResult<Word>;
    fn get_words(&self, text: &[String]) -> DbResult<Vec<Word>>;
    fn update_ending(&self, text: &str, new_end: &str) -> DbResult<()>;
    fn update_syl(&self, text: &str, new_syl: usize) -> DbResult<()>;
}

pub(crate) trait IPhraseRepo<C>: IDbRepo + INewDbRepo<C> {
    fn add_phrases(&self, phrases: &[NewPhrase]) -> DbResult<()>;
    fn get_by_id(&self, id: usize) -> DbResult<Phrase>;
    fn get_by_slug(&self, slug: &str) -> DbResult<Phrase>;
    fn get_by_text(&self, text: &str) -> DbResult<Phrase>;
    fn update_ending(&self, id: usize, new_end: &str) -> DbResult<()>;
    fn update_last_word(&self, id: usize, new_word: &str) -> DbResult<()>;
    fn update_syl(&self, id: usize, syl: usize) -> DbResult<()>;
    fn get_id_range(&self, min_id: usize, size: usize) -> DbResult<Option<(usize, usize)>>;
}

pub(crate) trait IRhymeRepo<C>: IDbRepo + INewDbRepo<C> {
    fn recalculate_rhymes(&self, batch_size: usize, max_syl_diff: usize) -> DbResult<()>;

    fn get_rhyme_to_phrase(
        &self,
        phrase: &Phrase,
        request: GetRhymeRequest,
    ) -> DbResult<Option<Phrase>>;

    fn get_random_phrase(&self, request: GetPhraseRequest) -> DbResult<Option<Phrase>>;
}
