use std::fs::File;
use std::path::Path;

use crate::entities::errors::DLResult;
use crate::entities::fun_iterator::FunIterator;
use crate::entities::lang::Lang;
use crate::entities::phrase::NewPhrase;
use crate::entities::word::Word;

pub trait ISpecificDictLoader<T> {
    fn lang(&self) -> Lang;
    fn ext(&self) -> &'static str;
    fn parse_file(&self, file: File) -> FunIterator<DLResult<T>>;
}

pub trait IUniversalDictLoader {
    fn load_words(&self, path: &Path, lang: Option<Lang>) -> DLResult<FunIterator<DLResult<Word>>>;
    fn load_phrases(
        &self,
        path: &Path,
        lang: Option<Lang>,
    ) -> DLResult<FunIterator<DLResult<NewPhrase>>>;
    fn extensions_words(&self) -> Vec<(Lang, Vec<&'static str>)>;
    fn extensions_phrases(&self) -> Vec<(Lang, Vec<&'static str>)>;
}
