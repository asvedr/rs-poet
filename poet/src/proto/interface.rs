use crate::entities::db_settings::BaseSettings;
use crate::entities::errors::DbResult;
use crate::entities::lang::Lang;
use crate::entities::phrase::{NewPhrase, Phrase};
use crate::entities::requests::{GetPhraseRequest, GetRhymeRequest};
use crate::entities::word::Word;

pub trait IBaseLoader<Connect>: Sized {
    fn create(connect: Connect, lang: Lang, description: &str) -> DbResult<Self>;

    fn load(connect: Connect) -> DbResult<Self>;
}

pub trait IWordBase {
    fn get_settings(&self) -> DbResult<BaseSettings>;
    fn get_size(&self) -> DbResult<usize>;
    fn add_words(&mut self, words: &[Word]) -> DbResult<()>;
    fn get_word(&self, text: &str) -> DbResult<Word>;
    fn get_words(&self, texts: &[String]) -> DbResult<Vec<Word>>;
    fn update_syl(&mut self, text: &str, new_syl: usize) -> DbResult<()>;
    fn update_ending(&mut self, text: &str, new_end: &str) -> DbResult<()>;
}

pub trait IPhraseBase {
    fn get_settings(&self) -> DbResult<BaseSettings>;
    fn get_phrases_size(&self) -> DbResult<usize>;
    fn get_rhymes_size(&self) -> DbResult<usize>;

    fn add_phrases(&mut self, phrases: &[NewPhrase]) -> DbResult<()>;
    fn get_by_slug(&self, slug: &str) -> DbResult<Phrase>;
    fn get_by_text(&self, text: &str) -> DbResult<Phrase>;
    fn update_ending(&mut self, id: usize, new_end: &str) -> DbResult<()>;
    fn update_last_word(&mut self, id: usize, new_word: &str) -> DbResult<()>;
    fn update_syl(&mut self, id: usize, syl: usize) -> DbResult<()>;

    fn recalculate_rhymes(&mut self, batch_size: usize, max_syl_diff: usize) -> DbResult<()>;
    fn get_rhyme_to_phrase(
        &self,
        phrase: &Phrase,
        request: GetRhymeRequest,
    ) -> DbResult<Option<Phrase>>;
    fn get_random_phrase(&self, request: GetPhraseRequest) -> DbResult<Option<Phrase>>;
}
