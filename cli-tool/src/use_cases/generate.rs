use clidi::cli::validators::{AsIsValidator, FromStrValidator};
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use json::object::Object;
use json::{parse, JsonValue};
use poet::{create_generator, load_phrase_base, PoetError, RowSizeSchema, VerseSchema};
use std::fs::File;
use std::io::Read;
use std::str::FromStr;

const DEFAULT_RETIRES: usize = 10;

#[derive(Default)]
pub struct GenerateUC {}

pub struct Request {
    phrase_db: String,
    schema_path: String,
    retry_count: usize,
}

impl GenerateUC {
    fn load_file(path: String) -> Result<String, UseCaseError> {
        let mut file = File::open(path).map_err(UseCaseError::make_runtime_d)?;
        let mut buf = String::new();
        file.read_to_string(&mut buf)
            .map_err(UseCaseError::make_runtime_d)?;
        Ok(buf)
    }

    fn get_key<'a>(js: &'a Object, key: &str) -> Result<&'a JsonValue, UseCaseError> {
        match js.get(key) {
            Some(val) => Ok(val),
            _ => UseCaseError::raise_runtime(format!("key {} not found", key)),
        }
    }

    fn parse_size(src: &JsonValue) -> Result<RowSizeSchema, UseCaseError> {
        if let Some(val) = src.as_usize() {
            return Ok(RowSizeSchema::Fixed(val));
        }
        let text = match src.as_str() {
            Some(val) => val,
            _ => return UseCaseError::raise_runtime(format!("invalid size: {:?}", src)),
        };
        if text.starts_with('l') {
            Self::parse_relative_size(text)
        } else {
            Self::parse_range_size(text)
        }
    }

    fn parse_relative_size(text: &str) -> Result<RowSizeSchema, UseCaseError> {
        let src = text.chars().skip(1).collect::<String>();
        let row_index = usize::from_str(&src)
            .map_err(|_| UseCaseError::make_runtime(format!("invalid relative size: {}", text)))?;
        Ok(RowSizeSchema::Relative {
            row_index,
            size_change: 0,
        })
    }

    fn parse_range_size(text: &str) -> Result<RowSizeSchema, UseCaseError> {
        let split = text.split('-').collect::<Vec<_>>();
        if split.len() != 2 {
            return UseCaseError::raise_runtime(format!("invalid range size: {}", text));
        }
        let (from, to) = match (usize::from_str(split[0]), usize::from_str(split[1])) {
            (Ok(f), Ok(t)) => (f, t),
            _ => return UseCaseError::raise_runtime(format!("invalid range size: {}", text)),
        };
        Ok(RowSizeSchema::Range { from, to })
    }

    fn parse_sizes(src: &JsonValue) -> Result<Vec<RowSizeSchema>, UseCaseError> {
        let list = match src {
            JsonValue::Array(val) => val,
            _ => return UseCaseError::raise_runtime("sizes is not a list"),
        };
        list.iter()
            .map(Self::parse_size)
            .collect::<Result<Vec<_>, _>>()
    }

    fn parse_rhyme(src: &JsonValue) -> Result<(usize, usize), UseCaseError> {
        let list = match src {
            JsonValue::Array(val) => val,
            _ => return UseCaseError::raise_runtime("rhyme is not a list"),
        };
        if list.len() != 2 {
            return UseCaseError::raise_runtime("rhyme is not a list of 2 item");
        }
        match (list[0].as_usize(), list[1].as_usize()) {
            (Some(a), Some(b)) => Ok((a, b)),
            _ => UseCaseError::raise_runtime("rhyme is not a list numbers"),
        }
    }

    fn parse_rhymes(src: &JsonValue) -> Result<Vec<(usize, usize)>, UseCaseError> {
        let list = match src {
            JsonValue::Array(val) => val,
            _ => return UseCaseError::raise_runtime("rhymes is not a list"),
        };
        list.iter()
            .map(Self::parse_rhyme)
            .collect::<Result<Vec<_>, _>>()
    }

    fn parse_schema(src: String) -> Result<VerseSchema, UseCaseError> {
        let js = parse(&src).map_err(UseCaseError::make_runtime_d)?;
        let js = match js {
            JsonValue::Object(val) => val,
            _ => return UseCaseError::raise_runtime("schema is not a dict"),
        };
        let sizes = Self::parse_sizes(Self::get_key(&js, "sizes")?)?;
        let rhymes = Self::parse_rhymes(Self::get_key(&js, "rhymes")?)?;
        Ok(VerseSchema { sizes, rhymes })
    }
}

impl ICliUseCase for GenerateUC {
    type Request = Request;

    fn execute(&mut self, request: Self::Request) -> Result<(), UseCaseError> {
        let base = load_phrase_base(&request.phrase_db).map_err(UseCaseError::make_runtime_d)?;
        let mut generator = create_generator(base).map_err(UseCaseError::make_runtime_d)?;
        let schema_src = Self::load_file(request.schema_path)?;
        let schema = Self::parse_schema(schema_src)?;
        match generator.generate_verse(&schema, request.retry_count) {
            Ok(val) => println!("{}", val),
            Err(PoetError::MatchedRowsNotFound) => eprintln!("Can not generate"),
            Err(err) => return Err(UseCaseError::make_runtime_d(err)),
        }
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("generate verse using schema")
            .param(Param::new::<_, AsIsValidator>("phrase_db"))
            .param(Param::new::<_, AsIsValidator>("schema_path"))
            .param(
                Param::new::<_, FromStrValidator<usize>>("retry_count")
                    .default(DEFAULT_RETIRES.to_string()),
            )
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            phrase_db: raw.get("phrase_db"),
            schema_path: raw.get("schema_path"),
            retry_count: raw.get("retry_count"),
        }
    }
}
