use clidi::entities::{CliUseCaseMeta, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;

use poet::version;

#[derive(Default)]
pub struct VersionUC {}

impl ICliUseCase for VersionUC {
    type Request = ();

    fn execute(&mut self, _: ()) -> Result<(), UseCaseError> {
        println!(
            "cli version: {}\npoet_version: {}",
            env!("CARGO_PKG_VERSION"),
            version(),
        );
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("get version information")
    }

    fn validate_request(&self, _: RawUseCaseRequest) -> Self::Request {}
}
