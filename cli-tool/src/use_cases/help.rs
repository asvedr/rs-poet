use clidi::entities::{CliUseCaseMeta, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;

const TEXT: &str = r#"
Use -h to see the list of commands
Workflow:
    - create empty word base "words new"
    - fill word base
        OR fill word base from dict(s) "words load"
        OR fill word base from csv(test,syl,ending) "words import"
    - create empty phrase base "phrases new"
    - fill phrase base
        OR fill base from raw data "phrases load"
           raw data is separated by \n lines of text
        OR import base from csv "phrases import"
           csv(slug,text,ending,last_word,syl_count)
           word base is not required for this step
           each item in csv is encoded: "\s" => "\", "\c" => ","
    - use phrase base for generation
"#;

#[derive(Default)]
pub struct HelpUC {}

impl ICliUseCase for HelpUC {
    type Request = ();

    fn execute(&mut self, _: ()) -> Result<(), UseCaseError> {
        println!("{}", TEXT);
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("show workflow help")
    }

    fn validate_request(&self, _: RawUseCaseRequest) -> Self::Request {}
}
