use clidi::cli::validators::{AsIsValidator, FromStrValidator};
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::load_word_base;

#[derive(Default)]
pub struct UpdateWordUC {}

pub struct Request {
    db_path: String,
    text: String,
    ending: Option<String>,
    syl: Option<usize>,
}

impl ICliUseCase for UpdateWordUC {
    type Request = Request;

    fn execute(&mut self, request: Self::Request) -> Result<(), UseCaseError> {
        let mut base = load_word_base(&request.db_path).map_err(UseCaseError::make_runtime_d)?;
        let word = base
            .get_word(&request.text)
            .map_err(UseCaseError::make_runtime_d)?;
        if let Some(val) = request.ending {
            base.update_ending(&word.text, &val)
                .map_err(UseCaseError::make_runtime_d)?;
        }
        if let Some(val) = request.syl {
            base.update_syl(&word.text, val)
                .map_err(UseCaseError::make_runtime_d)?;
        }
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("update word attrs")
            .param(Param::new::<_, AsIsValidator>("db_path"))
            .param(Param::new::<_, AsIsValidator>("text"))
            .param(
                Param::new::<_, AsIsValidator>("ending")
                    .short("e")
                    .description("new ending")
                    .optional(true),
            )
            .param(
                Param::new::<_, FromStrValidator<usize>>("syl")
                    .short("s")
                    .description("new syl count")
                    .optional(true),
            )
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            db_path: raw.get("db_path"),
            text: raw.get("text"),
            ending: raw.get_opt("ending"),
            syl: raw.get_opt("syl"),
        }
    }
}
