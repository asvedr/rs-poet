use clidi::cli::validators::AsIsValidator;
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::{load_word_base, DbError};

#[derive(Default)]
pub struct GetWordUC {}

pub struct Request {
    db_path: String,
    text: String,
}

impl ICliUseCase for GetWordUC {
    type Request = Request;

    fn execute(&mut self, request: Self::Request) -> Result<(), UseCaseError> {
        let base = load_word_base(&request.db_path).map_err(UseCaseError::make_runtime_d)?;
        match base.get_word(&request.text) {
            Ok(word) => println!("{:?}", word),
            Err(DbError::NotFound) => println!("not found"),
            Err(err) => return Err(UseCaseError::make_runtime_d(err)),
        }
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("get word with details")
            .param(Param::new::<_, AsIsValidator>("db_path"))
            .param(Param::new::<_, AsIsValidator>("text"))
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            db_path: raw.get("db_path"),
            text: raw.get("text"),
        }
    }
}
