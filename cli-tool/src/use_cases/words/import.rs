use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str::FromStr;

use clidi::cli::validators::{AsIsValidator, FromStrValidator};
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::{load_word_base, Word};

const DEFAULT_CHUNK_SIZE: usize = 100;

#[derive(Default)]
pub struct ImportWbUC {}

pub struct Request {
    db_path: String,
    text_path: String,
    chunk_size: usize,
}

impl ImportWbUC {
    fn make_word(src: &str) -> Option<Word> {
        let line = src.split(',').collect::<Vec<_>>();
        if line.len() != 3 {
            return None;
        }
        Some(Word {
            text: line[0].to_string(),
            syl_count: usize::from_str(line[1]).ok()?,
            ending: line[2].to_string(),
        })
    }

    fn get_next_chunk(
        file: &mut BufReader<File>,
        chunk_size: usize,
    ) -> Result<Vec<Word>, UseCaseError> {
        let mut result = Vec::new();
        let mut buffer = String::new();
        for _ in 0..chunk_size {
            buffer.clear();
            file.read_line(&mut buffer)
                .map_err(UseCaseError::make_runtime)?;
            if buffer.is_empty() {
                break;
            }
            let stripped = buffer.trim();
            if stripped.is_empty() {
                continue;
            }
            if let Some(word) = Self::make_word(stripped) {
                result.push(word)
            } else {
                let msg = format!("Invalid line: {:?}", stripped);
                return UseCaseError::raise_runtime(msg);
            }
        }
        Ok(result)
    }
}

impl ICliUseCase for ImportWbUC {
    type Request = Request;

    fn execute(&mut self, request: Self::Request) -> Result<(), UseCaseError> {
        let mut base = load_word_base(&request.db_path).map_err(UseCaseError::make_runtime_d)?;
        let file = File::open(request.text_path).map_err(UseCaseError::make_runtime)?;
        let mut file = BufReader::new(file);
        loop {
            let words = Self::get_next_chunk(&mut file, request.chunk_size)?;
            if words.is_empty() {
                break;
            }
            base.add_words(&words)
                .map_err(UseCaseError::make_runtime_d)?;
        }
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("load new words to dict from csv 'word,syl,ending' format")
            .param(Param::new::<_, AsIsValidator>("db_path"))
            .param(Param::new::<_, AsIsValidator>("text_path"))
            .param(
                Param::new::<_, FromStrValidator<usize>>("chunk_size")
                    .default(DEFAULT_CHUNK_SIZE.to_string()),
            )
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            db_path: raw.get("db_path"),
            text_path: raw.get("text_path"),
            chunk_size: raw.get("chunk_size"),
        }
    }
}
