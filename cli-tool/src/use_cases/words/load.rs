use clidi::cli::validators::{AsIsValidator, FromStrValidator};
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::{create_dict_loader, load_word_base};

const DEFAULT_CHUNK_SIZE: usize = 100;

#[derive(Default)]
pub struct LoadWbUC {}

pub struct Request {
    db_path: String,
    text_path: String,
    chunk_size: usize,
}

impl ICliUseCase for LoadWbUC {
    type Request = Request;

    fn execute(&mut self, request: Self::Request) -> Result<(), UseCaseError> {
        let mut base = load_word_base(&request.db_path).map_err(UseCaseError::make_runtime_d)?;
        let loader = create_dict_loader();
        let settings = base.get_settings().map_err(UseCaseError::make_runtime_d)?;
        let iterator = loader
            .load_words(request.text_path.as_ref(), Some(settings.lang))
            .map_err(UseCaseError::make_runtime)?;
        let mut buffer = Vec::new();
        for item in iterator {
            let word = item.map_err(UseCaseError::make_runtime)?;
            buffer.push(word);
            if buffer.len() >= request.chunk_size {
                base.add_words(&buffer)
                    .map_err(UseCaseError::make_runtime_d)?;
                buffer.clear();
            }
        }
        base.add_words(&buffer)
            .map_err(UseCaseError::make_runtime_d)?;
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("load new words to dict")
            .param(Param::new::<_, AsIsValidator>("db_path"))
            .param(Param::new::<_, AsIsValidator>("text_path"))
            .param(
                Param::new::<_, FromStrValidator<usize>>("chunk_size")
                    .default(DEFAULT_CHUNK_SIZE.to_string()),
            )
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            db_path: raw.get("db_path"),
            text_path: raw.get("text_path"),
            chunk_size: raw.get("chunk_size"),
        }
    }
}
