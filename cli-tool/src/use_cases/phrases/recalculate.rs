use clidi::cli::validators::{AsIsValidator, FromStrValidator};
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::load_phrase_base;

use crate::utils::exec_with_time;

const REC_BATCH_SIZE_DEFAULT: usize = 100;
const REC_MAX_DIFF: usize = 2;

#[derive(Default)]
pub struct RecalculateUC {}

pub struct Request {
    db_path: String,
    rec_batch_size: usize,
    rec_max_diff: usize,
}

impl ICliUseCase for RecalculateUC {
    type Request = Request;

    fn execute(&mut self, request: Self::Request) -> Result<(), UseCaseError> {
        let mut base = load_phrase_base(&request.db_path).map_err(UseCaseError::make_runtime_d)?;
        println!("recalculating");
        let (time, res) = exec_with_time(|| {
            base.recalculate_rhymes(request.rec_batch_size, request.rec_max_diff)
        });
        println!("recalculated in {}", time);
        res.map_err(UseCaseError::make_runtime_d)?;
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("update phrase attrs")
            .param(Param::new::<_, AsIsValidator>("db_path"))
            .param(
                Param::new::<_, FromStrValidator<usize>>("rec_batch_size")
                    .short("rb")
                    .default(REC_BATCH_SIZE_DEFAULT.to_string()),
            )
            .param(
                Param::new::<_, FromStrValidator<usize>>("rec_max_diff")
                    .short("rm")
                    .default(REC_MAX_DIFF.to_string()),
            )
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            db_path: raw.get("db_path"),
            rec_batch_size: raw.get("rec_batch_size"),
            rec_max_diff: raw.get("rec_max_diff"),
        }
    }
}
