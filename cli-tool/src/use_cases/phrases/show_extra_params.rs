use clidi::cli::validators::AsIsValidator;
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::{create_phrase_processor, load_word_base};

#[derive(Default)]
pub struct ShowExtParamsUC {}

pub struct Request {
    word_base_path: String,
}

impl ICliUseCase for ShowExtParamsUC {
    type Request = Request;

    fn execute(&mut self, request: Self::Request) -> Result<(), UseCaseError> {
        let base = load_word_base(&request.word_base_path).map_err(UseCaseError::make_runtime_d)?;
        let processor = create_phrase_processor(base).map_err(UseCaseError::make_runtime_d)?;
        let params = processor
            .get_params()
            .map_err(UseCaseError::make_runtime_d)?;
        for (key, descr) in params {
            println!("{}: {}", key, descr);
        }
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("show extra params for phrase loader")
            .param(Param::new::<_, AsIsValidator>("word_base_path"))
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            word_base_path: raw.get("word_base_path"),
        }
    }
}
