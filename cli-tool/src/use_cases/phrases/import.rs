use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str::FromStr;

use clidi::cli::validators::{AsIsValidator, FromStrValidator};
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::{load_phrase_base, IPhraseBase, NewPhrase};

use crate::utils::exec_with_time;

const DEFAULT_CHUNK_SIZE: usize = 100;
const REC_BATCH_SIZE: usize = 100;
const REC_MAX_DIFF: usize = 2;

#[derive(Default)]
pub struct ImportPbUC {}

pub struct Request {
    phrases_db_path: String,
    text_path: String,
    chunk_size: usize,
    rec_batch_size: usize,
    rec_max_diff: usize,
}

impl ImportPbUC {
    fn process_file(
        base: &mut dyn IPhraseBase,
        mut file: BufReader<File>,
        chunk_size: usize,
    ) -> Result<(), UseCaseError> {
        let mut line = String::new();
        let mut buffer = Vec::new();
        let mut total = 0;
        loop {
            line.clear();
            file.read_line(&mut line)
                .map_err(UseCaseError::make_runtime)?;
            if line.is_empty() {
                break;
            }
            let stripped = line.trim();
            if stripped.is_empty() {
                continue;
            }
            buffer.push(Self::process_line(stripped)?);
            total += 1;
            if buffer.len() >= chunk_size {
                base.add_phrases(&buffer)
                    .map_err(UseCaseError::make_runtime_d)?;
                buffer.clear();
                println!("saved: {}", total);
            }
        }
        base.add_phrases(&buffer)
            .map_err(UseCaseError::make_runtime_d)?;
        println!("saved: {}", total);
        Ok(())
    }

    fn bad_line(line: &str) -> UseCaseError {
        let msg = format!("invalid line: {:?}", line);
        UseCaseError::RuntimeError(msg)
    }

    fn process_line(line: &str) -> Result<NewPhrase, UseCaseError> {
        let split = line.split(',').collect::<Vec<_>>();
        if split.len() != 5 {
            return Err(Self::bad_line(line));
        }
        let phrase = NewPhrase {
            slug: Self::deescape(split[0]),
            text: Self::deescape(split[1]),
            ending: Self::deescape(split[2]),
            last_word: Self::deescape(split[3]),
            syl_count: usize::from_str(split[4]).map_err(|_| Self::bad_line(line))?,
        };
        Ok(phrase)
    }

    fn deescape(line: &str) -> String {
        line.replace("\\s", "\\").replace("\\c", ",")
    }
}

impl ICliUseCase for ImportPbUC {
    type Request = Request;

    fn execute(&mut self, request: Request) -> Result<(), UseCaseError> {
        let mut base =
            load_phrase_base(&request.phrases_db_path).map_err(UseCaseError::make_runtime_d)?;
        println!("processing phrases");
        let file = File::open(request.text_path).map_err(UseCaseError::make_runtime)?;
        let buffer = BufReader::new(file);
        let (time, res) = {
            let link = &mut *base;
            exec_with_time(|| Self::process_file(link, buffer, request.chunk_size))
        };
        res?;
        println!("processed in {}", time);
        println!("running rhyme recalculation");
        let (time, res) = exec_with_time(|| {
            base.recalculate_rhymes(request.rec_batch_size, request.rec_max_diff)
        });
        res.map_err(UseCaseError::make_runtime_d)?;
        println!("recalculated in {}", time);
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("import phrases from csv")
            .param(Param::new::<_, AsIsValidator>("phrase_path").description("phrase base path"))
            .param(Param::new::<_, AsIsValidator>("text_path").description("path to data"))
            .param(
                Param::new::<_, FromStrValidator<usize>>("chunk_size")
                    .default(DEFAULT_CHUNK_SIZE.to_string()),
            )
            .param(
                Param::new::<_, FromStrValidator<usize>>("rec_batch_size")
                    .description("recalculation batch size")
                    .default(REC_BATCH_SIZE.to_string()),
            )
            .param(
                Param::new::<_, FromStrValidator<usize>>("rec_max_diff")
                    .description("recalculation max diff")
                    .default(REC_MAX_DIFF.to_string()),
            )
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            phrases_db_path: raw.get("phrase_path"),
            text_path: raw.get("text_path"),
            chunk_size: raw.get("chunk_size"),
            rec_batch_size: raw.get("rec_batch_size"),
            rec_max_diff: raw.get("rec_max_diff"),
        }
    }
}
