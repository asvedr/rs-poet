use clidi::cli::validators::AsIsValidator;
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::{load_phrase_base, DbError};

#[derive(Default)]
pub struct GetPhraseUC {}

pub struct Request {
    db_path: String,
    slug: String,
}

impl ICliUseCase for GetPhraseUC {
    type Request = Request;

    fn execute(&mut self, request: Self::Request) -> Result<(), UseCaseError> {
        let base = load_phrase_base(&request.db_path).map_err(UseCaseError::make_runtime_d)?;
        match base.get_by_slug(&request.slug) {
            Ok(word) => println!("{:?}", word),
            Err(DbError::NotFound) => println!("not found"),
            Err(err) => return Err(UseCaseError::make_runtime_d(err)),
        }
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("get phrase with details")
            .param(Param::new::<_, AsIsValidator>("db_path"))
            .param(Param::new::<_, AsIsValidator>("slug"))
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            db_path: raw.get("db_path"),
            slug: raw.get("slug"),
        }
    }
}
