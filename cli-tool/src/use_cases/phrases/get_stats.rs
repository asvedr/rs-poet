use clidi::cli::validators::AsIsValidator;
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::load_phrase_base;

#[derive(Default)]
pub struct GetStatsPbUC {}

pub struct Request {
    path: String,
}

impl ICliUseCase for GetStatsPbUC {
    type Request = Request;

    fn execute(&mut self, request: Request) -> Result<(), UseCaseError> {
        let base = load_phrase_base(&request.path).map_err(UseCaseError::make_runtime_d)?;
        let settings = base.get_settings().map_err(UseCaseError::make_runtime_d)?;
        println!("lang: {:?}", settings.lang);
        println!("description: {}", settings.description);
        println!("base version: {}", settings.base_version);
        println!("poet version: {}", settings.poet_version);
        let p_size = base
            .get_phrases_size()
            .map_err(UseCaseError::make_runtime_d)?;
        let r_size = base
            .get_rhymes_size()
            .map_err(UseCaseError::make_runtime_d)?;
        println!("phrases size: {}", p_size);
        println!("rhymes size: {}", r_size);
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("view db info").param(Param::new::<_, AsIsValidator>("path"))
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            path: raw.get("path"),
        }
    }
}
