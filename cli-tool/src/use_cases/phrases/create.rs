use clidi::cli::validators::{AsIsValidator, FromStrValidator};
use clidi::entities::{CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::{create_phrase_base, Lang};

#[derive(Default)]
pub struct CreatePbUC {}

pub struct Request {
    path: String,
    lang: Lang,
    description: String,
}

impl ICliUseCase for CreatePbUC {
    type Request = Request;

    fn execute(&mut self, request: Request) -> Result<(), UseCaseError> {
        create_phrase_base(&request.path, request.lang, &request.description)
            .map_err(UseCaseError::make_runtime_d)?;
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        CliUseCaseMeta::new("create empty phrase base")
            .param(Param::new::<_, AsIsValidator>("path"))
            .param(Param::new::<_, FromStrValidator<Lang>>("lang"))
            .param(Param::new::<_, AsIsValidator>("description").default(""))
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            path: raw.get("path"),
            lang: raw.get("lang"),
            description: raw.get("description"),
        }
    }
}
