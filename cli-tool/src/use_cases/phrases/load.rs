use std::collections::HashSet;
use std::fs::File;
use std::io::Write;

use clidi::cli::validators::{AsIsValidator, FromStrValidator};
use clidi::entities::cli::use_case::ArgParseResult;
use clidi::entities::{CliArgError, CliUseCaseMeta, Param, RawUseCaseRequest, UseCaseError};
use clidi::proto::cli::ICliUseCase;
use poet::{
    create_phrase_processor, load_phrase_base, load_word_base, DbError, FunIterator, IPhraseBase,
    IPhraseProcessor, NewPhrase, PhraseProcessorError,
};

use crate::utils::exec_with_time;

const DEFAULT_CHUNK_SIZE: usize = 100;
const REC_BATCH_SIZE: usize = 100;
const REC_MAX_DIFF: usize = 2;

#[derive(Default)]
pub struct LoadPbUC {}

pub struct Request {
    word_db_path: String,
    phrases_db_path: String,
    text_path: String,
    chunk_size: usize,
    extra: Vec<(String, String)>,
    unknown_out: Option<String>,
    rec_batch_size: usize,
    rec_max_diff: usize,
}

fn validate_extra(src: String) -> ArgParseResult {
    let mut result: Vec<(String, String)> = Vec::new();
    for split in src.split(';') {
        if split.is_empty() {
            continue;
        }
        let split = split.split(':').collect::<Vec<_>>();
        if split.len() != 2 {
            return Err(CliArgError::ExpectedValue("key:val;*".to_string()));
        }
        let item = (split[0].to_string(), split[1].to_string());
        result.push(item);
    }
    Ok(Box::new(result))
}

type Bases = (Box<dyn IPhraseBase>, Box<dyn IPhraseProcessor>);

impl LoadPbUC {
    fn init_bases(request: &Request) -> Result<Bases, DbError> {
        let base = load_word_base(&request.word_db_path)?;
        let w_lang = base.get_settings()?.lang;
        let processor = create_phrase_processor(base)?;
        let base = load_phrase_base(&request.phrases_db_path)?;
        let p_lang = base.get_settings()?.lang;
        if w_lang != p_lang {
            Err("word base and phrase base in different languages".into())
        } else {
            Ok((base, processor))
        }
    }

    fn process_iter(
        chunk_size: usize,
        base: &mut dyn IPhraseBase,
        iter: FunIterator<Result<NewPhrase, PhraseProcessorError>>,
    ) -> Result<(usize, HashSet<String>), UseCaseError> {
        let mut buffer = Vec::new();
        let mut unknown = HashSet::new();
        let mut saved = 0_usize;
        for item in iter {
            match item {
                Ok(val) => {
                    saved += 1;
                    buffer.push(val)
                }
                Err(PhraseProcessorError::UnknownWord(word)) => {
                    unknown.insert(word);
                }
                Err(err) => return Err(UseCaseError::make_runtime_d(err)),
            };
            if buffer.len() >= chunk_size {
                base.add_phrases(&buffer)
                    .map_err(UseCaseError::make_runtime_d)?;
                buffer.clear();
            }
        }
        base.add_phrases(&buffer)
            .map_err(UseCaseError::make_runtime_d)?;
        Ok((saved, unknown))
    }
}

impl ICliUseCase for LoadPbUC {
    type Request = Request;

    fn execute(&mut self, request: Request) -> Result<(), UseCaseError> {
        let (mut base, processor) =
            Self::init_bases(&request).map_err(UseCaseError::make_runtime_d)?;
        let iter = processor
            .load_file(request.text_path.as_ref(), request.extra)
            .map_err(UseCaseError::make_runtime_d)?;
        println!("processing phrases");
        let chunk_size = request.chunk_size;
        let (time, res) = exec_with_time(|| Self::process_iter(chunk_size, &mut *base, iter));
        println!("processed in {}", time);
        let (saved, unknown) = res?;
        println!("saved: {}", saved);
        println!("recalculating rhymes");
        let (time, res) = exec_with_time(|| {
            base.recalculate_rhymes(request.rec_batch_size, request.rec_max_diff)
        });
        res.map_err(UseCaseError::make_runtime_d)?;
        println!("completed in {}", time);
        if let Some(path) = request.unknown_out {
            let mut out = File::create(path).map_err(UseCaseError::make_runtime_d)?;
            for word in unknown {
                let _ = writeln!(out, "{}", word);
            }
        } else {
            println!("unknown words:");
            for word in unknown {
                println!("  {}", word);
            }
        }
        Ok(())
    }

    fn get_meta(&self) -> CliUseCaseMeta {
        let validator: Box<dyn Fn(String) -> ArgParseResult> = Box::new(validate_extra);
        CliUseCaseMeta::new("load phrases from text separated by \\n")
            .param(Param::new::<_, AsIsValidator>("words_path").description("word base path"))
            .param(Param::new::<_, AsIsValidator>("phrase_path").description("phrase base path"))
            .param(Param::new::<_, AsIsValidator>("text_path").description("path to data"))
            .param(
                Param::new::<_, FromStrValidator<usize>>("chunk_size")
                    .short("c")
                    .default(DEFAULT_CHUNK_SIZE.to_string()),
            )
            .param(
                Param::with_validator("extra", validator)
                    .short("e")
                    .description("params for filter")
                    .default(""),
            )
            .param(
                Param::new::<_, AsIsValidator>("unknown_path")
                    .short("u")
                    .description("path to store unknown words")
                    .optional(true),
            )
            .param(
                Param::new::<_, FromStrValidator<usize>>("rec_batch_size")
                    .short("rb")
                    .description("recalculation batch size")
                    .default(REC_BATCH_SIZE.to_string()),
            )
            .param(
                Param::new::<_, FromStrValidator<usize>>("rec_max_diff")
                    .short("rm")
                    .description("recalculation max diff")
                    .default(REC_MAX_DIFF.to_string()),
            )
    }

    fn validate_request(&self, raw: RawUseCaseRequest) -> Self::Request {
        Request {
            word_db_path: raw.get("words_path"),
            phrases_db_path: raw.get("phrase_path"),
            text_path: raw.get("text_path"),
            chunk_size: raw.get("chunk_size"),
            extra: raw.get("extra"),
            unknown_out: raw.get_opt("unknown_path"),
            rec_batch_size: raw.get("rec_batch_size"),
            rec_max_diff: raw.get("rec_max_diff"),
        }
    }
}
