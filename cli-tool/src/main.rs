use clidi::cli::{Runner, RunnerTree};

mod use_cases;
mod utils;

fn words() -> Runner {
    let create = Runner::new_leaf(
        || Ok(use_cases::words::create::CreateWbUC::default()),
        "create empty base",
        "new",
    );
    let load = Runner::new_leaf(
        || Ok(use_cases::words::load::LoadWbUC::default()),
        "add words to base(parse dict format)",
        "load",
    );
    let imp = Runner::new_leaf(
        || Ok(use_cases::words::import::ImportWbUC::default()),
        "import words from csv(no parse)",
        "import",
    );
    let get = Runner::new_leaf(
        || Ok(use_cases::words::get::GetWordUC::default()),
        "get word",
        "get",
    );
    let upd = Runner::new_leaf(
        || Ok(use_cases::words::update::UpdateWordUC::default()),
        "update word's ending or syl count",
        "upd",
    );
    let stats = Runner::new_leaf(
        || Ok(use_cases::words::get_stats::GetStatsWbUC::default()),
        "get words base stats",
        "stats",
    );
    RunnerTree::fold(
        vec![create, load, imp, get, upd, stats],
        "word base manipulations",
    )
    .into_runner("words")
}

fn phrases() -> Runner {
    let create = Runner::new_leaf(
        || Ok(use_cases::phrases::create::CreatePbUC::default()),
        "create empty phrase base",
        "new",
    );
    let stats = Runner::new_leaf(
        || Ok(use_cases::phrases::get_stats::GetStatsPbUC::default()),
        "get phrase base stats",
        "stats",
    );
    let load = Runner::new_leaf(
        || Ok(use_cases::phrases::load::LoadPbUC::default()),
        "load phrases from source",
        "load",
    );
    let lparams = Runner::new_leaf(
        || Ok(use_cases::phrases::show_extra_params::ShowExtParamsUC::default()),
        "show extra params for \"load\" command",
        "show-extra",
    );
    let imp = Runner::new_leaf(
        || Ok(use_cases::phrases::import::ImportPbUC::default()),
        "import phrases from csv",
        "import",
    );
    let get = Runner::new_leaf(
        || Ok(use_cases::phrases::get::GetPhraseUC::default()),
        "get phrase details",
        "get",
    );
    let upd = Runner::new_leaf(
        || Ok(use_cases::phrases::update::UpdatePhraseUC::default()),
        "update phrase",
        "upd",
    );
    let rec = Runner::new_leaf(
        || Ok(use_cases::phrases::recalculate::RecalculateUC::default()),
        "recalculate rhymes",
        "rec",
    );
    RunnerTree::fold(
        vec![create, load, lparams, imp, get, upd, stats, rec],
        "phrase base manupulations",
    )
    .into_runner("phrases")
}

fn main() {
    let ver = Runner::new_leaf(
        || Ok(use_cases::version::VersionUC::default()),
        "get version",
        "version",
    );
    let hlp = Runner::new_leaf(
        || Ok(use_cases::help::HelpUC::default()),
        "workflow help",
        "help",
    );
    let gen = Runner::new_leaf(
        || Ok(use_cases::generate::GenerateUC::default()),
        "generate verse",
        "gen",
    );
    RunnerTree::fold(vec![hlp, words(), phrases(), gen, ver], "poet cli tool")
        .into_main_runner()
        .run()
}
