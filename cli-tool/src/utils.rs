use std::time::SystemTime;

pub fn exec_with_time<T, F: FnOnce() -> T>(action: F) -> (u64, T) {
    let begin = SystemTime::now();
    let res = action();
    let end = SystemTime::now();
    let time = end.duration_since(begin).unwrap().as_secs();
    (time, res)
}
