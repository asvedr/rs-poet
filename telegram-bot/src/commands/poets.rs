use crate::config::Config;
use crate::errors::BotError;
use crate::proto::ICommand;

pub struct Command {
    authors: Vec<String>,
}

impl Command {
    pub fn new(config: &Config) -> Self {
        Self {
            authors: config.authors.clone(),
        }
    }
}

impl ICommand for Command {
    fn command(&self) -> &str {
        "/authors"
    }

    fn execute(&mut self, _: &str) -> Result<String, BotError> {
        Ok(self.authors.join(", "))
    }
}
