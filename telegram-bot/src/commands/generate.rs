use poet::{create_generator, load_phrase_base, IGenerator, RowSizeSchema, VerseSchema};
use std::collections::{HashMap, HashSet};
use std::path::Path;

use crate::config::Config;
use crate::errors::BotError;
use crate::proto::ICommand;

const MAX_TRIES: usize = 10;

pub struct PoetState {
    base_path: String,
    authors: HashSet<String>,
    poets: HashMap<String, Box<dyn IGenerator>>,
}

pub struct Command {
    state: PoetState,
    schema: VerseSchema,
}

impl PoetState {
    fn make_author<'a>(&'a mut self, name: &str) -> Result<&'a mut dyn IGenerator, BotError> {
        let path = Path::new(&self.base_path).join(format!("{}.db", name));
        let base = load_phrase_base(&path)?;
        let gen = create_generator(base)?;
        self.poets.insert(name.to_string(), gen);
        Ok(&mut **self.poets.get_mut(name).unwrap())
    }

    fn get_author<'a>(&'a mut self, name: &str) -> Result<&'a mut dyn IGenerator, BotError> {
        // if let Some(gen) = self.poets.get_mut(name) {
        if self.poets.contains_key(name) {
            Ok(&mut **self.poets.get_mut(name).unwrap())
        } else {
            self.make_author(name)
        }
    }
}

impl Command {
    pub fn new(config: &Config) -> Self {
        let authors = config.authors.iter().cloned().collect();
        Self {
            state: PoetState {
                base_path: config.poet_base_dir.clone(),
                authors,
                poets: HashMap::new(),
            },
            schema: VerseSchema {
                sizes: vec![
                    RowSizeSchema::Fixed(9),
                    RowSizeSchema::Fixed(8),
                    RowSizeSchema::Fixed(9),
                    RowSizeSchema::Fixed(8),
                ],
                rhymes: vec![(0, 2), (1, 3)],
            },
        }
    }
}

impl ICommand for Command {
    fn command(&self) -> &str {
        "/gen"
    }

    fn execute(&mut self, text: &str) -> Result<String, BotError> {
        let name = text.trim().to_lowercase();
        if !self.state.authors.contains(&name) {
            return Ok("unknown poet".to_string());
        }
        let author = self.state.get_author(&name)?;
        let verse = author.generate_verse(&self.schema, MAX_TRIES)?;
        Ok(verse)
    }
}
