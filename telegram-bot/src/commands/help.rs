use crate::errors::BotError;
use crate::proto::ICommand;

pub struct Command {
    command: &'static str,
}

impl Command {
    pub fn help() -> Self {
        Self { command: "/help" }
    }

    pub fn start() -> Self {
        Self { command: "/start" }
    }
}

impl ICommand for Command {
    fn command(&self) -> &str {
        self.command
    }

    fn execute(&mut self, _: &str) -> Result<String, BotError> {
        let msg = concat!(
            "Robot poet\nCommands:\n",
            "  /help - show this message\n",
            "  /authors - show poet list\n",
            "  /gen <author> - generate verse"
        );
        Ok(msg.to_string())
    }
}
