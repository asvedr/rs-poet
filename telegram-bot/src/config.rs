use mddd::macros::IConfig;
use mddd::traits::IParser;

struct ListParser;

impl IParser<Vec<String>> for ListParser {
    fn parse(src: &str) -> Result<Vec<String>, String> {
        let list = src
            .split(',')
            .map(|s| s.trim().to_string())
            .filter(|s| !s.is_empty())
            .collect();
        Ok(list)
    }
}

#[derive(IConfig)]
#[allow(clippy::manual_non_exhaustive)]
pub struct Config {
    #[description("telegram token")]
    pub token: String,
    #[parser(ListParser)]
    #[description("available authors")]
    pub authors: Vec<String>,
    #[name("BASE_DIR")]
    #[description("directory with poet bases")]
    pub poet_base_dir: String,
    #[description("file to save messages")]
    pub message_log: Option<String>,
    #[description("sleep in seconds")]
    #[default(5.0)]
    pub sleep_on_errors: f64,
    #[prefix("POET_")]
    __meta__: (),
}
