use poet::{DbError, PoetError};
use std::io::Error;

#[derive(Debug)]
pub enum BotError {
    CmdNotFound,
    TgError(telegram_bot_ars::Error),
    IOError(std::io::Error),
    PoetError(PoetError),
    DbError(DbError),
}

impl From<std::io::Error> for BotError {
    fn from(src: Error) -> Self {
        Self::IOError(src)
    }
}

impl From<telegram_bot_ars::Error> for BotError {
    fn from(src: telegram_bot_ars::Error) -> Self {
        Self::TgError(src)
    }
}

impl From<PoetError> for BotError {
    fn from(src: PoetError) -> Self {
        Self::PoetError(src)
    }
}

impl From<DbError> for BotError {
    fn from(src: DbError) -> Self {
        Self::DbError(src)
    }
}
