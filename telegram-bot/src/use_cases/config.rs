use mddd::traits::{IConfig, IUseCase};

use crate::config::Config;

pub struct ConfigUC;

impl IUseCase for ConfigUC {
    type Request = ();
    type Error = String;

    fn short_description() -> String {
        "show config params".to_string()
    }

    fn execute(&mut self, _: Self::Request) -> Result<(), Self::Error> {
        println!("{}", Config::help());
        Ok(())
    }
}
