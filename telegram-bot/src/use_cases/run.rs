use mddd::traits::IUseCase;
use std::thread;
use std::time::Duration;

use crate::bot;
use crate::config::Config;
use crate::proto::ICommand;

pub struct RunBotUC<'a> {
    pub config: &'a Config,
    pub commands: Vec<Box<dyn ICommand>>,
}

impl<'a> IUseCase for RunBotUC<'a> {
    type Request = ();
    type Error = String;

    fn short_description() -> String {
        "run bot".to_string()
    }

    fn execute(&mut self, _: ()) -> Result<(), Self::Error> {
        loop {
            let res = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(bot::run(self.config, &mut self.commands));
            if let Err(err) = res {
                eprintln!("TOPLEVEL ERROR: {:?}", err);
                thread::sleep(Duration::from_secs_f64(self.config.sleep_on_errors));
            }
        }
    }
}
