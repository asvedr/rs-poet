use mddd::std::{LeafRunner, NodeRunner};

mod app;
mod bot;
mod commands;
mod config;
mod errors;
mod proto;
mod use_cases;
mod utils;

fn main() {
    use app::use_cases as uc;

    NodeRunner::new("poet telegram bot")
        .add("run", LeafRunner::new(uc::run))
        .add("config", LeafRunner::new(uc::config))
        .set_default_params(&["run"])
        .run()
}
