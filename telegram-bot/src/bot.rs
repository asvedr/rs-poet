use std::fs::{File, OpenOptions};
use std::io::{self, Write};
use std::path::Path;

use futures::StreamExt;
use telegram_bot_ars::{Api, CanReplySendMessage, Message, MessageKind, UpdateKind};

use crate::config::Config;
use crate::errors::BotError;
use crate::proto::ICommand;
use crate::utils::normalize_input;

fn execute_command(
    commands: &mut [Box<dyn ICommand>],
    key: String,
    text: String,
) -> Result<String, BotError> {
    for command in commands {
        if command.command() == key {
            return command.execute(&text);
        }
    }
    Err(BotError::CmdNotFound)
}

fn escape(text: &str) -> String {
    text.replace('\\', "\\s").replace(',', "\\c")
}

fn log_message(file: &mut Option<File>, message: &Message, text: &str) -> Result<(), BotError> {
    let file = match file {
        Some(val) => val,
        _ => return Ok(()),
    };
    let columns = [
        message.from.id.to_string(),
        match message.from.username {
            None => "".to_string(),
            Some(ref val) => escape(val),
        },
        message.chat.id().to_string(),
        escape(text),
    ]
    .join(",");
    writeln!(file, "{}", columns)?;
    file.flush()?;
    Ok(())
}

fn get_log_file(path: &Path) -> io::Result<File> {
    if path.exists() {
        OpenOptions::new().write(true).append(true).open(path)
    } else {
        File::create(path)
    }
}

pub async fn run(config: &Config, commands: &mut [Box<dyn ICommand>]) -> Result<(), BotError> {
    let mut msg_logger = match config.message_log {
        None => None,
        Some(ref val) => {
            let file = get_log_file(Path::new(val))?;
            Some(file)
        }
    };
    let api = Api::new(&config.token);

    // Fetch new updates via long poll method
    let mut stream = api.stream();
    while let Some(update) = stream.next().await {
        // If the received update contains a new message...
        let update = update?;
        let message = match update.kind {
            Some(UpdateKind::Message(val)) => val,
            _ => continue,
        };
        let text = match message.kind {
            MessageKind::Text { ref data, .. } => data,
            _ => continue,
        };
        println!("> {}", text);
        let (cmd, value) = normalize_input(text);
        let text = match execute_command(commands, cmd, value) {
            Ok(val) => val,
            Err(BotError::CmdNotFound) => {
                log_message(&mut msg_logger, &message, text)?;
                continue;
            }
            Err(err) => {
                eprintln!("\nERROR:\n  msg:{}\nerr: {:?}", text, err,);
                format!("ERROR: {:?}", err)
            }
        };
        if text.is_empty() {
            continue;
        }
        api.send(message.text_reply(text)).await?;
    }
    Ok(())
}
