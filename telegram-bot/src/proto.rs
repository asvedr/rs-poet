use crate::errors::BotError;

pub trait ICommand {
    fn command(&self) -> &str;
    fn execute(&mut self, text: &str) -> Result<String, BotError>;
}
