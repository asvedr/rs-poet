use crate::app::components;
use crate::use_cases::config::ConfigUC;
use crate::use_cases::run::RunBotUC;

pub fn run() -> RunBotUC<'static> {
    RunBotUC {
        config: components::config(),
        commands: components::commands(),
    }
}

pub fn config() -> ConfigUC {
    ConfigUC
}
