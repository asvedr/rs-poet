use mddd::macros::singleton_simple;
use mddd::traits::IConfig;

use crate::commands;
use crate::config::Config;
use crate::proto::ICommand;

singleton_simple!(
    pub fn config() -> Config {
        match Config::build() {
            Ok(val) => val,
            Err(err) => panic!("Config error: {:?}", err),
        }
    }
);

pub fn commands() -> Vec<Box<dyn ICommand>> {
    vec![
        Box::new(commands::help::Command::start()),
        Box::new(commands::help::Command::help()),
        Box::new(commands::poets::Command::new(config())),
        Box::new(commands::generate::Command::new(config())),
    ]
}
