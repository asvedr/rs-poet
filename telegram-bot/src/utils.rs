const SPACES: &str = "\n\t\r\0";

pub fn normalize_input(text: &str) -> (String, String) {
    let mut normalized = String::new();
    for sym in text.trim().chars() {
        if SPACES.contains(sym) {
            normalized.push(' ')
        } else {
            normalized.push(sym)
        }
    }
    let mut split = normalized.split(' ');
    let first_token = split.next().unwrap().to_string();
    let rest = split.collect::<Vec<_>>().join(" ");
    (first_token, rest)
}
