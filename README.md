# Verse generator
## Components:
  - poet - core library
  - cli-tool - CLI utils for data preparation & tests
  - telegram-bot - telegram robot for data usage
## CLI Tool:
Tool can generate database which can be used for generation.
1) generate base of words
2) generate base of phrases
3) use `poet-cli gen` to check result
4) include phrase base in tg bot
  - `poet-cli help` - check workflow
  - `poet-cli -h` show commands
